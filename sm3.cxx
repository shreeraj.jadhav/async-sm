#include <chrono>
#include <functional>
#include <future>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include <comm/broker.h>
#include <comm/service.h>
#include <comm/zmq.h>

#include "thirdparty/cxxopts/include/cxxopts.hpp"
#include <comm/loop_executor.h>
#include <stlab/concurrency/immediate_executor.hpp>

#define LOGURU_WITH_STREAMS 1
//#define LOGURU_IMPLEMENTATION 1
#include "thirdparty/loguru/loguru.hpp"

int main(int argc, char* argv[])
{
  // Turn off individual parts of the preamble
  loguru::g_preamble_date = false; // The date field
  loguru::g_preamble_time = false; // The time of the current day
  loguru::init(argc, argv);
  LOG_S(INFO) << "Starting process...";

  // init zero mq.
  comm::GetZMQContext();

  //---------------------------------------------------------------------------
  // BROKER
  //---------------------------------------------------------------------------
  auto retvalB = std::async(std::launch::async, []() {
    comm::Broker broker;
    return broker.Exec("tcp://*:11111");
  });
  using namespace std::chrono_literals;
  std::this_thread::sleep_for(1s);

  //---------------------------------------------------------------------------
  // DATA-SERVER
  //---------------------------------------------------------------------------
  auto retvalDS = std::async(std::launch::async, []() {
    comm::Service ds("data-server");
    ds.Connect("tcp://localhost:11111");

    auto executor = comm::loop_executor_type{};
    auto recv = ds.GetMainReceiver();
    auto hold = recv | stlab::executor(executor) & [&](const comm::Message& msg) {
      LOG_S(INFO) << "got req (will reply)  " << &msg << msg.size();
      comm::Message reply;
      ds.SendReply(msg, std::move(reply));
    };
    LOG_S(INFO) << "set_ready";
    recv.set_ready();

    auto recv2 = ds.GetMainReceiver();
    auto hold2 = recv2 | stlab::executor(executor) & [&](const comm::Message& msg) {
      LOG_S(INFO) << "got req (will notify) " << &msg << msg.size();
      comm::Message reply;
      ds.Publish("notifications", std::move(reply));
    };
    LOG_S(INFO) << "set_ready";
    recv2.set_ready();

    executor.loop();
  });

  //---------------------------------------------------------------------------
  // CLIENT
  //---------------------------------------------------------------------------
  comm::Service client("client");
  client.Connect("tcp://localhost:11111");
  auto receiver = client.Subscribe("notifications");

  auto executor = comm::loop_executor_type{};
  auto hold = receiver |
    stlab::executor(executor) & [](const comm::Message& msg) { LOG_S(INFO) << "got notification"; };

  auto hold2 = receiver |
    stlab::executor(executor) & [](const comm::Message& msg) { LOG_S(INFO) << "got notification"; };

  receiver.set_ready();

  comm::Message frames;
  auto hold3 = client.SendRequest("data-server", std::move(frames))
                 .then(executor, [](const comm::Message& msg) {
                   LOG_S(INFO) << "got reply for `SendRequest` " << msg.size();
                 });

  std::this_thread::sleep_for(4s);
  client.TerminateAll();
  executor.loop();

  LOG_S(INFO) << "done";
  return EXIT_SUCCESS;
}
