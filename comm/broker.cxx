#include "broker.h"

#include "message.h"
#include "zmq.h"

#include <iterator>
#include <zmq.hpp>

#include "comm.pb.h"

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

namespace comm
{

class Broker::BInternals
{
  std::map<std::string, std::vector<Message> > ServiceEnvelopes;
  std::map<std::string, std::vector<Message> > SubscriptionEnvelopes;
  bool WaitingForTermination = false;

public:
  comm::GreetingResponse DoGreetingRequest(
    const comm::GreetingRequest& req, const Message& incoming)
  {
    // clone all frames except the footer and the separator.
    Message frames_clone =
      incoming.Clone(incoming.begin(), std::next(incoming.begin(), incoming.size() - 1));
    assert(frames_clone.back()->size() == 0);
    frames_clone.pop_back();

    this->ServiceEnvelopes[req.service_name()].push_back(std::move(frames_clone));
    LOG_S(INFO) << "registered service for '" << req.service_name() << "'";
    comm::GreetingResponse res;
    res.set_status(true);
    return res;
  }

  bool DoGenericRequest(
    const comm::GenericRequest& req, const Message& incoming, zmq::socket_t& endpoint)
  {
    bool handled = false;
    for (const auto& dest : req.destinations())
    {
      auto seiter = this->ServiceEnvelopes.find(dest);
      if (seiter == this->ServiceEnvelopes.end() || seiter->second.size() == 0)
      {
        LOG_S(INFO) << "no service found for '" << dest << "'";
      }
      else
      {
        LOG_S(1) << "forward to " << dest << " (count=" << seiter->second.size() << ")";
        for (const auto& senv : seiter->second)
        {
          auto clone = senv.Clone();
          clone.AppendClone(incoming);
          comm::send(std::move(clone), endpoint);
          handled = true;
        }
      }
    }
    return handled;
  }

  void Subscribe(const comm::Subscribe& req, Message&& msg, zmq::socket_t& endpoint)
  {
    const auto channel = req.channel();
    LOG_S(INFO) << "registered subscription for '" << channel << "'";
    msg.ClearData(/*remove_delimiter=*/true);
    if (!this->WaitingForTermination)
    {
      this->SubscriptionEnvelopes[channel].push_back(std::move(msg));
    }
    else
    {
      auto env_pair = msg.GetEnvelope();
      Message data = Message::Clone(env_pair.first, env_pair.second);
      comm::TerminationRequest treq;
      comm::Footer footer;
      footer.mutable_body()->PackFrom(treq);
      data.AppendEmptyFrame();
      data.Append(footer);
      comm::send(std::move(data), endpoint);
    }
  }

  void Publish(const comm::Publish& pub, Message&& msg, zmq::socket_t& endpoint) const
  {
    const auto channel = pub.channel();
    if (this->SubscriptionEnvelopes.find(channel) == this->SubscriptionEnvelopes.end())
    {
      LOG_S(1) << "No subscriptions for " << channel << ". Ignoring.";
      return;
    }

    LOG_S(1) << "broadcasting message for '" << channel << "' " << msg.DebugString();
    msg.ClearEnvelope(/*remove_delimiter*/ true);

    for (const auto& senv : this->SubscriptionEnvelopes.at(channel))
    {
      auto env_pair = senv.GetEnvelope();

      Message data = Message::Clone(env_pair.first, env_pair.second);
      data.AppendEmptyFrame();
      data.AppendClone(msg);
      comm::send(std::move(data), endpoint);
    }
  }

  bool DoTerminationRequest(const Message& incoming, zmq::socket_t& endpoint)
  {
    bool handled = true;
    // Send termination notice to subscriptions
    Message sub_msg = incoming;
    sub_msg.ClearEnvelope(/*remove_delimiter*/ true);
    for (const auto& sbiter : this->SubscriptionEnvelopes)
    {
      if (sbiter.second.size() != 0)
      {
        LOG_S(1) << "forward to " << sbiter.first << " (count=" << sbiter.second.size() << ")";
        for (const auto& senv : sbiter.second)
        {
          auto env_pair = senv.GetEnvelope();
          Message data = Message::Clone(env_pair.first, env_pair.second);
          data.AppendEmptyFrame();
          data.AppendClone(sub_msg);
          handled = handled && comm::send(std::move(data), endpoint);
        }
      }
    }
    // Send termination notice to services
    for (const auto& seiter : this->ServiceEnvelopes)
    {
      if (seiter.second.size() != 0)
      {
        LOG_S(1) << "forward to " << seiter.first << " (count=" << seiter.second.size() << ")";
        for (const auto& senv : seiter.second)
        {
          auto clone = senv.Clone();
          clone.AppendClone(incoming);
          handled = handled && comm::send(std::move(clone), endpoint);
        }
      }
    }

    this->WaitingForTermination = true;
    return handled;
  }

  bool DoTerminationResponse(const comm::Footer& footer)
  {
    // We only track response from services, since pipes
    // are associated with services. Subsribers terminate
    // without sending response.
    comm::TerminationResponse tres;
    footer.body().UnpackTo(&tres);
    if (tres.status())
    {
      this->ServiceEnvelopes.erase(tres.source());
      if (this->ServiceEnvelopes.empty())
      {
        // Last service has terminated.
        return true;
      }
    }

    // Don't terminate broker yet.
    return false;
  }
};

//-----------------------------------------------------------------------------
Broker::Broker()
  : Internals(new Broker::BInternals())
{
}

//-----------------------------------------------------------------------------
Broker::~Broker()
{
}

//-----------------------------------------------------------------------------
int Broker::Exec(const std::string& url)
{
  auto& internals = (*this->Internals);

  loguru::set_thread_name("broker");

  zmq::socket_t endpoint{ comm::GetZMQContext(), ZMQ_ROUTER };
  endpoint.bind(url);
  LOG_S(INFO) << "waiting on '" << url << "'";

  comm::Footer footer;
  while (true)
  {
    auto message_frames = comm::recv(endpoint);
    // parse footer and then decide the action.
    if (!comm::z2p(*message_frames.back(), &footer))
    {
      LOG_S(INFO) << "message has no valid footer!!! Ignoring. " << message_frames.DebugString();
      continue;
    }

    if (footer.body().Is<comm::GreetingRequest>())
    {
      LOG_S(1) << "got comm::GreetingRequest";
      comm::GreetingRequest greq;
      footer.body().UnpackTo(&greq);

      auto response = internals.DoGreetingRequest(greq, message_frames);
      footer.mutable_body()->PackFrom(response);

      message_frames.ClearData();
      message_frames.Append(footer);
      comm::send(std::move(message_frames), endpoint);
    }
    else if (footer.body().Is<comm::GenericRequest>())
    {
      LOG_S(1) << "got comm::GenericRequest";
      comm::GenericRequest greq;
      footer.body().UnpackTo(&greq);
      if (!internals.DoGenericRequest(greq, message_frames, endpoint))
      {
        // reply back failure.
        comm::GenericResponse res;
        res.set_source("broker");
        res.set_status(false);
        footer.mutable_body()->PackFrom(res);

        message_frames.ClearData();
        message_frames.Append(footer);
        comm::send(std::move(message_frames), endpoint);
      }
    }
    else if (footer.body().Is<comm::GenericResponse>())
    {
      LOG_S(1) << "got comm::GenericResponse";

      // remove enveloppe (including delimiter) and send the message
      message_frames.ClearEnvelope(/*remove_delimiter=*/true);
      comm::send(std::move(message_frames), endpoint);
    }
    else if (footer.body().Is<comm::Subscribe>())
    {
      LOG_S(1) << "got comm::Subscribe";
      comm::Subscribe sreq;
      footer.body().UnpackTo(&sreq);
      internals.Subscribe(sreq, std::move(message_frames), endpoint);
    }
    else if (footer.body().Is<comm::Publish>())
    {
      LOG_S(1) << "got comm::Publish";
      comm::Publish spub;
      footer.body().UnpackTo(&spub);
      internals.Publish(spub, std::move(message_frames), endpoint);
    }
    else if (footer.body().Is<comm::TerminationRequest>())
    {
      LOG_S(1) << "got comm::TerminationRequest";
      if (!internals.DoTerminationRequest(message_frames, endpoint))
      {
        // reply back failure.
        comm::GenericResponse res;
        res.set_source("broker");
        res.set_status(false);
        footer.mutable_body()->PackFrom(res);

        message_frames.ClearData();
        message_frames.Append(footer);
        comm::send(std::move(message_frames), endpoint);
      }
    }
    else if (footer.body().Is<comm::TerminationResponse>())
    {
      if (internals.DoTerminationResponse(footer))
      {
        LOG_S(INFO) << "All services terminated: broker is terminating.";
        break;
      }
    }
    else
    {
      LOG_S(INFO) << "unhandled message: " << footer.ShortDebugString();
    }
  }

  return EXIT_SUCCESS;
}
}
