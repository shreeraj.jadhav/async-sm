#include "message.h"
#include "comm.pb.h"

#include <sstream>

namespace comm
{
//-----------------------------------------------------------------------------
Message::Message()
{
}

//-----------------------------------------------------------------------------
Message::~Message()
{
}

//-----------------------------------------------------------------------------
void Message::Append(zmq::message_t&& msg)
{
  auto pmsg = std::make_shared<zmq::message_t>(std::move(msg));
  this->Frames.push_back(pmsg);
}

//-----------------------------------------------------------------------------
void Message::AppendEmptyFrame()
{
  this->Append(zmq::message_t(size_t(0)));
}

//-----------------------------------------------------------------------------
void Message::Append(const google::protobuf::Message& msg)
{
  comm::MessageWrapper wrapper;
  wrapper.mutable_content()->PackFrom(msg);

  size_t size = wrapper.ByteSizeLong();
  std::unique_ptr<char[]> data(new char[size]);
  wrapper.SerializeToArray(data.get(), static_cast<int>(size));
  this->Append(zmq::message_t(data.get(), size));
}

//-----------------------------------------------------------------------------
void Message::AppendClone(const zmq::message_t& msg)
{
  zmq::message_t clone;
  clone.copy(&msg);
  this->Append(std::move(clone));
}

//-----------------------------------------------------------------------------
void Message::Append(Message&& msg)
{
  for (auto& msg : msg.Frames)
  {
    this->Frames.push_back(msg);
  }
  msg.Frames.clear();
}

//-----------------------------------------------------------------------------
void Message::AppendClone(const Message& msg)
{
  for (const auto& msg : msg.Frames)
  {
    this->AppendClone(*msg.get());
  }
}

//-----------------------------------------------------------------------------
Message Message::Clone(Message::const_iterator b, Message::const_iterator e)
{
  Message clone;
  for (auto iter = b; iter != e; ++iter)
  {
    clone.AppendClone(*iter->get());
  }
  return clone;
}

//-----------------------------------------------------------------------------
void Message::ClearData(bool remove_delimiter)
{
  for (auto iter = this->Frames.begin(); iter != this->Frames.end(); ++iter)
  {
    if (iter->get()->size() == 0)
    {
      if (!remove_delimiter)
      {
        ++iter;
      }
      this->Frames.erase(iter, this->Frames.end());
      return;
    }
  }
}

//-----------------------------------------------------------------------------
void Message::ClearEnvelope(bool remove_delimiter)
{
  auto ipair = this->GetEnvelope();
  if (remove_delimiter && ipair.second != this->Frames.end())
  {
    ipair.second++;
  }
  this->Frames.erase(ipair.first, ipair.second);
}

//-----------------------------------------------------------------------------
std::pair<Message::const_iterator, Message::const_iterator> Message::GetEnvelope() const
{
  return std::make_pair(this->Frames.begin(), std::find_if(this->Frames.begin(), this->Frames.end(),
                                                [](const std::shared_ptr<zmq::message_t>& msg) {
                                                  return msg->size() == 0;
                                                }));
}

//-----------------------------------------------------------------------------
std::pair<Message::iterator, Message::iterator> Message::GetEnvelope()
{
  return std::make_pair(this->Frames.begin(), std::find_if(this->Frames.begin(), this->Frames.end(),
                                                [](const std::shared_ptr<zmq::message_t>& msg) {
                                                  return msg->size() == 0;
                                                }));
}

//-----------------------------------------------------------------------------
std::string Message::DebugString() const
{
  std::ostringstream str;
  str << "Message [ ";
  for (const auto& ptr : *this)
  {
    str << ptr->size() << " ";
  }
  str << "]";
  return str.str();
}

} // namespace comm
