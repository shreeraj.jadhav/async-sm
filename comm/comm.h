#ifndef comm_comm_h
#define comm_comm_h

/**
 * @namespace comm
 *
 * `comm` namespace encapsulates low level communication components. These
 * implement the communication protocol atop ZeroMQ sockets that forms the
 * backbone for inter-service communication in our design.
 *
 * The communication is inspired by ZMQ's Majordomo protocol (MDP) with several
 * differences to satisfy ParaView specific use-cases. Hence we call this
 * protocol PMP (ParaView's Majordomo Protocol).
 *
 * There are following components in PMP.
 *
 * 1. Services: These are services that ask for work to be done or do the work.
 *              These are the clients and servers. PMP makes no distinction
 *              between clients and servers and treats them similarly.
 *              @sa comm::Service
 *
 * 2. Broker:   This is the component that is the main communication junction
 *              between various services. There can be one and only one broker in
 *              our network topology. All services connect to the broker (via
 *              forwarders/pipes) and communication is routed by the broker.
 *              @sa comm::Broker
 *
 * 3. Message:  All data exchanged has a footer data frame that is a protobuf message.
 *              @sa comm::Message
 *
 * Service
 * -------
 *
 * A Service is anything that does work or request work to be done. Each
 * service is identified by a name. Multiple services can have same name, in
 * which case they are treated as mirrors (we may revisit this).
 *
 * A Service *MUST* use a **DEALER** socket to connect to the Broker.
 *
 */
namespace comm
{
}

#endif
