#include "service.h"
#include "loop_executor.h"

#include "comm.pb.h"
#include "zmq.h"
#include <zmq.hpp>

#include <queue>
#define LOGURU_IMPLEMENTATION 1
#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <stlab/concurrency/default_executor.hpp>
#include <stlab/concurrency/utility.hpp>

namespace comm
{

namespace detail
{

struct priority_queue_item
{
  priority_queue_item(std::uint64_t idx, comm::Message&& msg)
    : index(idx)
    , message(msg)
  {
  }

  bool operator>(const priority_queue_item& other) const { return this->index > other.index; }

  std::uint64_t index;
  mutable comm::Message message;
};

// a service pipe is a used by a Service internally to avoid
// connecting a remote broker
class Pipe
{
public:
  Pipe(const std::string brokerUrl)
    : BrokerURL(brokerUrl)
  {
    std::ostringstream str;
    str << reinterpret_cast<std::int64_t>(this);
    this->FrontendURL = std::string("inproc://f-") + str.str();
    this->ControlURL = std::string("inproc://c-") + str.str();
  }

  ~Pipe() { this->Terminate(); }

  const std::string& GetFrontendURL() const { return this->FrontendURL; }
  stlab::receiver<Message> GetReceiver() const { return this->Receiver; }
  void SendReply(Message&& msg) const
  {
    assert(this->CtrlEndpoint != nullptr);

    Message reply;
    reply.AppendEmptyFrame();
    reply.Append(std::move(msg));

    // TODO: validate being called on same thread as the one that owns the
    // CtrlEndpoint.
    comm::send(std::move(reply), *this->CtrlEndpoint);
  }

  bool Start(const std::string& serviceName)
  {
    assert(this->CtrlEndpoint == nullptr);

    this->CtrlEndpoint.reset(new zmq::socket_t(comm::GetZMQContext(), ZMQ_DEALER));
    this->CtrlEndpoint->bind(this->ControlURL);

    auto& receiver = this->Receiver;
    stlab::sender<Message> sender;
    std::tie(sender, receiver) = stlab::channel<Message>(stlab::immediate_executor);
    receiver.set_ready();

    this->ExecStatus =
      std::async(std::launch::async, Pipe::Exec, serviceName, std::this_thread::get_id(),
        this->BrokerURL, this->FrontendURL, this->ControlURL, std::move(sender));

    auto frames = comm::recv(*this->CtrlEndpoint);

    comm::Footer footer;
    if (frames.size() == 1 && comm::z2p(*frames.back(), &footer) &&
      footer.body().Is<comm::GreetingResponse>())
    {
      comm::GreetingResponse gres;
      footer.body().UnpackTo(&gres);
      return gres.status();
    }
    return false;
  }

  //-----------------------------------------------------------------------------
  void Terminate()
  {
    LOG_S(INFO) << "Sending termination request to pipe-thread.";
    // construct comm::TerminateService message,
    // that will be passed to the control socket of the pipe.
    comm::TerminationRequest req;
    comm::Footer footer;
    footer.mutable_body()->PackFrom(req);

    // add the footer to the message.
    Message message, frames;
    message.AppendEmptyFrame();
    message.Append(std::move(frames));
    message.Append(footer);

    // we need ZMQ_DONTWAIT here in case the pipe has already terminated.
    comm::send(std::move(message), *this->CtrlEndpoint, ZMQ_DONTWAIT);
  }

private:
  static bool Exec(const std::string sname, const std::thread::id sid, const std::string burl,
    const std::string furl, const std::string curl, stlab::sender<Message>&& sender)
  {
    loguru::set_thread_name(("pipe:" + sname).c_str());

    zmq::socket_t backend(comm::GetZMQContext(), ZMQ_DEALER);
    backend.connect(burl);

    zmq::socket_t frontend(comm::GetZMQContext(), ZMQ_ROUTER);
    frontend.bind(furl);

    zmq::socket_t ctrl(comm::GetZMQContext(), ZMQ_DEALER);
    ctrl.connect(curl);

    // send greeting, get response and report back via ctrl.
    comm::GreetingRequest greeting;
    greeting.set_service_name(sname);

    comm::Footer footer;
    footer.mutable_body()->PackFrom(greeting);

    comm::Message frames;
    frames.AppendEmptyFrame();
    frames.Append(footer);
    footer.Clear();

    LOG_S(1) << "sending greeting.";
    comm::send(std::move(frames), backend);
    // todo: poll-n-wait with repeated "greetings" until success or failure.
    frames = comm::recv(backend);

    bool all_well = false;
    if (frames.size() > 0 && comm::z2p(*frames.back(), &footer) &&
      footer.body().Is<comm::GreetingResponse>())
    {
      comm::GreetingResponse gres;
      footer.body().UnpackTo(&gres);
      all_well = gres.status();
      LOG_S(1) << "greeting status: " << all_well;
    }
    else
    {
      comm::GreetingResponse gres;
      gres.set_status(false);
      footer.mutable_body()->PackFrom(gres);
      LOG_S(1) << "invalid greeting response!";
    }

    frames.clear();
    frames.Append(footer);
    comm::send(std::move(frames), ctrl);
    if (!all_well)
    {
      LOG_S(INFO) << "terminate";
      return false;
    }

    // piping messages from the frontend to the server
    // TODO: poll ctrl too.
    zmq::pollitem_t items[] = {
      { static_cast<void*>(backend), 0, ZMQ_POLLIN, 0 },
      { static_cast<void*>(frontend), 0, ZMQ_POLLIN, 0 },
      { static_cast<void*>(ctrl), 0, ZMQ_POLLIN, 0 },
    };

    LOG_S(INFO) << "polling...";
    footer.Clear();

    std::priority_queue<priority_queue_item, std::vector<priority_queue_item>,
      std::greater<priority_queue_item> >
      outgoing_queue;
    std::uint64_t next_outgoing_message_index = 0;
    while (zmq::poll(items, 3, -1) != -1)
    {
      if (items[0].revents & ZMQ_POLLIN)
      {
        // msg on backend.
        auto frames = comm::recv(backend);

        if (frames.size() > 0 && comm::z2p(*frames.back(), &footer))
        {
          if (footer.body().Is<comm::GenericRequest>())
          {
            comm::GenericRequest req;
            footer.body().UnpackTo(&req);

            LOG_S(1) << "got request";
            // todo: remove envelope and send data frames OR
            //       leave envelope and let receiver use envelope to reply.
            // must remove the footer, since the footer is not known to anyone
            // but the internals of the `comm` layer.
            frames.pop_back();

            if (req.skip_response())
            {
              // remove envelope so that we don't attempt to send any response
              // back.
              frames.ClearEnvelope(/*remove_delimiter=*/false);
            }
            sender(frames);
            // todo: pass receiver to get replies.
          }
          else if (footer.body().Is<comm::GenericResponse>())
          {
            LOG_S(1) << "got response";
            comm::send(std::move(frames), frontend);
          }
          else if (footer.body().Is<comm::Publish>())
          {
            LOG_S(1) << "got `pub` " << frames.DebugString();
            comm::send(std::move(frames), frontend);
          }
          else if (footer.body().Is<comm::TerminationRequest>())
          {
            LOG_S(1) << "got termination-request.";
            frames.Append(footer);
            // sender(frames);
            comm::send(std::move(frames), frontend);
            auto executor = comm::loop_executor_type{};
            executor.make_loop_exit(sid);
          }
          else
          {
            LOG_S(INFO) << "got unhandled! " << footer.ShortDebugString();
          }
        }
      }
      if (items[1].revents & ZMQ_POLLIN)
      {
        // msg on frontend.
        // forward to backend, in order.
        auto msg = comm::recv(frontend);
        auto indexframe = msg.back();
        assert(indexframe->size() == sizeof(std::uint64_t));
        std::uint64_t index = *reinterpret_cast<std::uint64_t*>(indexframe->data());
        msg.pop_back();
        outgoing_queue.push(priority_queue_item(index, std::move(msg)));
        while (!outgoing_queue.empty() && next_outgoing_message_index == outgoing_queue.top().index)
        {
          comm::send(std::move(outgoing_queue.top().message), backend);
          outgoing_queue.pop();
          ++next_outgoing_message_index;
        }
      }
      if (items[2].revents & ZMQ_POLLIN)
      {
        // msg on ctrl
        auto frames = comm::recv(ctrl);
        LOG_S(2) << "got ctrl message";
        // check if termination request
        if (frames.size() > 0 && comm::z2p(*frames.back(), &footer))
        {
          if (footer.body().Is<comm::TerminationRequest>())
          {
            LOG_S(2) << "recieved termination request...";
            LOG_S(2) << "sending termination response...";
            comm::TerminationResponse tres;
            tres.set_source(sname);
            tres.set_status(true);
            comm::Footer footer;
            footer.mutable_body()->PackFrom(tres);
            Message message;
            message.AppendEmptyFrame();
            message.Append(footer);
            comm::send(std::move(message), backend);
            break;
          }
        }
        // TODO: if reply, forward to backend.
        comm::send(std::move(frames), backend);
      }
    }

    // zmq::proxy(static_cast<void*>(frontend), static_cast<void*>(backend), nullptr);
    LOG_S(INFO) << "pipe terminated";
    return true;
  }

  std::string BrokerURL;
  std::string FrontendURL;
  std::string ControlURL;
  std::thread::id ServiceThreadId;

  std::unique_ptr<zmq::socket_t> CtrlEndpoint;
  std::future<bool> ExecStatus;
  stlab::receiver<Message> Receiver;
};
}

class Service::SInternals
{
public:
  SInternals(const std::string& name)
    : Name(name)
    , OutgoingMessageSequencer(0)
    , Active(false)
    , WaitingForReplies(0)
  {
  }
  ~SInternals() {}

  std::string Name;
  std::unique_ptr<detail::Pipe> Pipe;
  std::atomic<std::uint64_t> OutgoingMessageSequencer;
  std::vector<std::future<void> > SubscriptionFutures;
  std::atomic_bool Active;
  std::atomic<std::int64_t> WaitingForReplies;
};

//-----------------------------------------------------------------------------
Service::Service(const std::string& name)
  : Internals(new Service::SInternals(name))
{
  loguru::set_thread_name(name.c_str());
}

//-----------------------------------------------------------------------------
Service::~Service()
{
  this->Internals->Active = false;
  while(this->Internals->WaitingForReplies > 0){}
}

//-----------------------------------------------------------------------------
const std::string Service::GetName() const
{
  return this->Internals->Name;
}

//-----------------------------------------------------------------------------
bool Service::Connect(const std::string& brokerurl)
{
  auto& internals = (*this->Internals);
  if (internals.Pipe == nullptr)
  {
    LOG_S(INFO) << "connecting to '" << brokerurl << "'";

    internals.Pipe.reset(new detail::Pipe(brokerurl));
    if (internals.Pipe->Start(internals.Name))
    {
      LOG_S(INFO) << "connected to '" << brokerurl << "'";
      internals.Active = true;
      return true;
    }
    LOG_S(INFO) << "connection failed!";
    internals.Pipe.reset();
    internals.Active = false;
  }
  return false;
}

//-----------------------------------------------------------------------------
void Service::TerminateAll() const
{
  auto& internals = (*this->Internals);
  if (internals.Pipe != nullptr)
  {
    LOG_S(INFO) << "sending termination-all request to broker.";
    comm::TerminationRequest req;
    // req.add_destinations("broker");
    // req.set_skip_response(false);
    comm::Footer footer;
    footer.mutable_body()->PackFrom(req);

    // add the footer to the message.
    Message message, frames;
    message.AppendEmptyFrame();
    message.Append(std::move(frames));
    message.Append(footer);

    const auto index = internals.OutgoingMessageSequencer++;
    message.Append(zmq::message_t(&index, sizeof(std::uint64_t)));

    zmq::socket_t endpoint(comm::GetZMQContext(), ZMQ_DEALER);
    endpoint.connect(internals.Pipe->GetFrontendURL());
    LOG_S(1) << "sending message (" << index << ")";
    comm::send(std::move(message), endpoint);
  }
}

//-----------------------------------------------------------------------------
stlab::future<Message> Service::SendRequest(const std::string& service, Message&& frames) const
{
  auto& internals = (*this->Internals);
  if (internals.Pipe != nullptr)
  {
    // LOG_S(INFO) << "sending request to '" << service << "'";
    // add comm::GenericRequest message.
    comm::GenericRequest req;
    req.add_destinations(service);
    req.set_skip_response(false);
    comm::Footer footer;
    footer.mutable_body()->PackFrom(req);

    // add the footer to the message.
    Message message;
    message.AppendEmptyFrame();
    message.Append(std::move(frames));
    message.Append(footer);

    zmq::socket_t endpoint(comm::GetZMQContext(), ZMQ_DEALER);
    endpoint.connect(internals.Pipe->GetFrontendURL());
    std::uint64_t index = internals.OutgoingMessageSequencer++;
    LOG_S(1) << "sending request (" << index << ")";
    message.Append(zmq::message_t(&index, sizeof(std::uint64_t)));
    comm::send(std::move(message), endpoint);

    return stlab::async(stlab::default_executor,
      [&](zmq::socket_t&& endpoint, std::string serviceName, std::uint64_t msgNum) {
        // todo: handle disconnection?
        internals.WaitingForReplies++;
        LOG_S(1) << "waiting for response (" << msgNum << ")";
        zmq::pollitem_t items[]={ { static_cast<void*>(endpoint), 0, ZMQ_POLLIN, 0 } };
        // update service counter for tracking the number of threads that are waiting for reply.
        while (zmq::poll(items, 1, 1000) != -1)
        {
          if (items[0].revents & ZMQ_POLLIN)
          {
            Message rframes = comm::recv(endpoint);
            LOG_S(1) << "got response (" << msgNum << ") " << rframes.size();
            rframes.ClearEnvelope(/*remove_delimiter=*/true);
            assert(rframes.size() > 0);
            // footer in the frames received is comm::GenericResponse
            // todo: validate footer.
            // would you validate footer here, or let the requesting
            // service handle failed responses?
            // rframes.pop_back();
            internals.WaitingForReplies--;
            return rframes;
          }
          else if (!internals.Active)
          {
            internals.WaitingForReplies--;
            std::ostringstream err;
            err << "service: " << serviceName << " already terminated.";
            LOG_S(INFO) << err.str();
            throw err.str();
          }
        }
        // Ideally, we should never get here.
        internals.WaitingForReplies--;
        throw std::string( "reply never received.");
      },
      std::move(endpoint), internals.Name, index);
  }

  return stlab::make_ready_future(Message(), stlab::immediate_executor);
}

//-----------------------------------------------------------------------------
void Service::SendMessage(const std::string& service, Message&& frames) const
{
  auto& internals = (*this->Internals);
  if (internals.Pipe == nullptr)
  {
    return;
  }

  // add comm::GenericRequest message.
  comm::GenericRequest req;
  req.add_destinations(service);
  req.set_skip_response(true); // we don't want response back.

  comm::Footer footer;
  footer.mutable_body()->PackFrom(req);

  // add the footer to the message.
  Message message;
  message.AppendEmptyFrame();
  message.Append(std::move(frames));
  message.Append(footer);

  const auto index = internals.OutgoingMessageSequencer++;
  message.Append(zmq::message_t(&index, sizeof(std::uint64_t)));

  zmq::socket_t endpoint(comm::GetZMQContext(), ZMQ_DEALER);
  endpoint.connect(internals.Pipe->GetFrontendURL());
  LOG_S(1) << "sending message (" << index << ")";
  comm::send(std::move(message), endpoint);
}

//-----------------------------------------------------------------------------
void Service::SendReply(const Message& replyTo, Message&& reply) const
{
  // FIXME: this is not thread safe.
  auto& internals = (*this->Internals);
  assert(internals.Pipe != nullptr);

  // Copy envelope from `replyTo`.
  auto iterpair = replyTo.GetEnvelope();
  if (iterpair.first == iterpair.second)
  {
    // attempt to send a reply back for a message, ignore it.
    // note, this is not a error; it's by design.
    return;
  }
  Message msg = Message::Clone(iterpair.first, iterpair.second);
  assert(msg.size() > 0); // envelope cannot be empty!

  msg.AppendEmptyFrame();
  msg.Append(std::move(reply));

  comm::Footer footer;
  comm::GenericResponse response;
  response.set_status(true);
  footer.mutable_body()->PackFrom(response);
  msg.Append(footer);
  internals.Pipe->SendReply(std::move(msg));
}

//-----------------------------------------------------------------------------
stlab::receiver<Message> Service::Subscribe(const std::string& channel) const
{
  auto& internals = (*this->Internals);
  assert(internals.Pipe != nullptr);

  stlab::sender<Message> send;
  stlab::receiver<Message> receive;

  std::tie(send, receive) = stlab::channel<Message>(stlab::immediate_executor);

  auto f = std::async(std::launch::async,
    [](stlab::sender<Message>&& send, const std::string channel, const std::string furl,
                        std::uint64_t index) {

      LOG_S(INFO) << "subscribe to '" << channel << "'";
      zmq::socket_t endpoint(comm::GetZMQContext(), ZMQ_DEALER);
      endpoint.connect(furl);

      comm::Subscribe sub;
      sub.set_channel(channel);

      comm::Footer footer;
      footer.mutable_body()->PackFrom(sub);

      Message frames;
      frames.AppendEmptyFrame();
      frames.Append(footer);
      frames.Append(zmq::message_t(&index, sizeof(std::uint64_t)));
      comm::send(std::move(frames), endpoint);
      while (true) // todo: break when done.
      {
        Message rframes = comm::recv(endpoint);

        // Process termination request for channel subscriptions.
        footer.Clear();
        if (rframes.size() > 0 && comm::z2p(*rframes.back(), &footer) &&
          footer.body().Is<comm::TerminationRequest>())
        {
          LOG_S(9) << "terminating channel subscription.";
          break;
        }

        rframes.ClearEnvelope(/*remove_delimiter=*/true);
        // remove footer.
        rframes.pop_back();
        LOG_S(9) << "subscription received " << rframes.DebugString();
        send(rframes);
      }
    },
    std::move(send), channel, internals.Pipe->GetFrontendURL(),
    internals.OutgoingMessageSequencer++);
  // TODO: this detach is not good.
  internals.SubscriptionFutures.push_back(std::move(f));
  return receive;
}

//-----------------------------------------------------------------------------
bool Service::Publish(const std::string& channel, Message&& frames) const
{
  auto& internals = (*this->Internals);
  if (internals.Pipe != nullptr)
  {
    comm::Publish pub;
    pub.set_channel(channel);

    comm::Footer footer;
    footer.mutable_body()->PackFrom(pub);

    Message message;
    message.AppendEmptyFrame();
    message.Append(std::move(frames));
    message.Append(footer);

    auto index = internals.OutgoingMessageSequencer++;
    message.Append(zmq::message_t(&index, sizeof(std::uint64_t)));

    zmq::socket_t endpoint(comm::GetZMQContext(), ZMQ_DEALER);
    endpoint.connect(internals.Pipe->GetFrontendURL());
    return comm::send(std::move(message), endpoint);
  }
  return false;
}

//-----------------------------------------------------------------------------
stlab::receiver<Message> Service::GetMainReceiver() const
{
  auto& internals = (*this->Internals);
  assert(internals.Pipe != nullptr);
  return internals.Pipe->GetReceiver();
}

} // namespace comm
