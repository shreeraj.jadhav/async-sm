#ifndef comm_service_h
#define comm_service_h

#include <future>
#include <memory>

#include "message.h"
#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/future.hpp>

namespace comm
{
/**
 * @class service
 * @brief service implementation
 *
 * Service represents a component that does work or requests work to be done.
 * Thus, it can represent both client and servers in a client-server framework;
 * there is no distinction between the two.
 *
 * A service connects to the broker (`comm::Broker`) and exchanges messages with
 * other services via the broker. The service API is deliberately bare-bones and
 * provides building blocks to developer richer components. Next, we look at the
 * various components of the Service API.
 *
 * Every service has a name. The name is specified in the constructor.
 * Currently, name is simply a string. We may revise that in the future.
 *
 * The service must connect to a broker before any of its other API can be used.
 * That can be done using `Service::Connect` with the broker URL passed as the
 * argument. The broker can be on the same process (different thread) or
 * different process. Internally, `Service::Connect` launches an internal thread that
 * acts as a communication pipe to the talk with broker. This is an implementation
 * detail and can be safely ignored by most users/developers.
 *
 * On connecting to the broker, the Service sends a `comm::GreetingRequest`
 * message to register the service with the broker. `Service::Connect` blocks
 * till the registration is successful. In future, we may return a `future`.
 *
 * Service offers two ways to exchange messages: request-reply, or
 * publish-subscribe.
 *
 * Request-Reply
 * -------------
 *
 * These represent the most basic communication pattern where a service sends a
 * *request* to another service and expects a *reply* back from that service.
 * This is done by calling `Service::SendRequest`. This is an async call i.e.
 * the method does not block till the response is received from the target
 * service. Instead, it returns a `future` that the caller code can either wait
 * on or add a continuation. There are no restrictions or requirements on either
 * the request message or the response i.e. there is no need to add any
 * footer/header frames to the message. The request (or response) message can
 * have any many frames as determined by the application.
 *
 * Here are a few snippets how to access results from a SendRequest invocation.
 *
 * @code{cpp}
 *
 * comm::Message req = ...;
 *
 * // Block till the result is available (use sparingly, if at all).
 * auto f = service.SendRequest("target-service-name", std::move(req));
 * comm::Message reply = stlab::blocking_get(f);
 *
 * // Use a continuation.
 * auto f = service.SendRequest("target-service-name", std::move(req));
 * auto hold = f.then([](const comm::Message& reply){
 *    // do stuff with reply.
 * });
 *
 * note, `hold` is a `stlab::future` and must be held as long as a your are
 * interested in the continuation i.e. if the `hold` gets out of scope, then the
 * callback function in `then` won't get called. You can use
 * `stlab::future<T>::detach` let the continuation proceed even if the future
 * gets out of scope.
 *
 * // Use a continuation with detach
 * service.SendRequest("target-service-name", std::move(req))
 *          .then(
 *            [](const comm::Message& reply){
 *              // do stuff with reply.
 *              })
 *            .detach();
 *
 * @endcode
 *
 * While continuations using `then()` are handy, an important question becomes
 * which thread is the code in the continuation running on? Generally, you
 * want that code to run on the same thread that the request was made i.e the
 * same thread calling the `service.SendRequest`. This can be done by using the
 * `stlab::extensions::loop_executor` as follows.
 *
 * @code{cpp}
 *
 * auto f = service.SendRequest("target-service-name", std::move(req));
 *
 * auto executor = stlab::extensions::loop_executor_type{};
 * auto hold = f.then(executor, [](const comm::Message& reply){ ... });
 *
 * // run the even loop
 * executor.loop();
 *
 * @endcode
 *
 * `stlab::executor::loop_executor_type::loop` will process all events in the
 * current thread.
 *
 * You can combine executor too, e.g. to some heavy data processing in a
 * different thread and then some follow-on work on the event loop, do the
 * following:
 *
 * @code{cpp}
 * auto f = service.SendRequest("target-service-name", std::move(req));
 *
 * auto loop_executor = stlab::extensions::loop_executor_type{};
 *
 * auto hold = f.then(stlab::default_executor,
 *                    [](const comm::Message& reply) {
 *                      // do thread-safe work
 *                      return true;
 *                    }).then(stlab::loop_executor,
 *                    [](bool) {
 *                      // do work in main-loop.
 *                    });
 *
 * loop_executor.loop();
 *
 * @endcode
 *
 * A service that responds to requests needs to provide a "handler" to respond.
 * This is done by using `Service::GetMainReceiver`. `GetMainReceiver` returns
 * a `stlab::received<comm::Message>` that one can add process objects to do
 * work on each message received.
 *
 * @code{cpp}
 *
 * auto loop_executor = stlab::extensions::loop_executor_type{};
 * auto recv = service.GetMainReceiver();
 * auto hold = recv | stlab::executor(loop_executor) & [&](const comm::Message& msg) {
 *    comm::Message reply;
 *    reply.AppendEmptyFrame();
 *    service.SendReply(msg, std::move(reply));
 *  };
 *
 * // this is important to start receiving messages.
 * recv.set_ready();
 *
 * ...
 *
 * loop_executor.loop();
 *
 * @endcode
 *
 * Request/No-Reply
 * ----------------
 *
 * A variation of the Request-Reply is to make a request but ignore the reply.
 * For such cases, use `Service::SendMessage` instead of `SendRequest`. On the
 * receiving end, the same handlers are for `SendRequest` are invoked. Any calls
 * to `SendReply` in repose, however, are simply ignored.
 *
 * Publish/Subscribe
 * -----------------
 *
 * Pub/Sub provides a communication mechanism where a service can post a message
 * on a named channel and all services that have subscribed to that channel
 * receive that message.
 *
 * To publish a message to a channel, use `Service::Publish`.
 *
 * @code{cpp}
 *
 * comm::Message msg = ...
 * service.Publish("channel-name", std::move(msg));
 *
 * @endcode
 *
 * To subscribe to a channel and process messages on it, use the
 * `Service::Subscribe`. The method returns a `stlab::receiver<comm::Message>`
 * instance. One can add process objects to it similar to the receiver returned
 * by `Service::GetMainReceiver`.
 *
 * @code{cpp}
 *
 * auto receiver = service.Subscribe("notifications");
 *
 * auto executor = stlab::extensions::loop_executor_type{};
 * auto hold = receiver |
 *   stlab::executor(executor) & [](const comm::Message& msg) { LOG_S(INFO) << "got notification";
 * };
 *
 * auto hold2 = receiver |
 *   stlab::executor(executor) & [](const comm::Message& msg) { LOG_S(INFO) << "got notification";
 * };
 *
 * receiver.set_ready();
 *
 * @endcode
 *
 * Messages
 * ---------
 *
 * All messages sent and received on a Service are of the type comm::Message
 * which is a collection of zmq::message_t instances, called frames. A message
 * can have zero or more frames. The message passed to any of the message
 * sending APIs gets padded with a header (empty frame) and a footer (binary
 * representation of `comm::Footer`). These paddings are used internally by the
 * Service and Broker to route and handle the message.
 *
 * Message Order
 * -------------
 *
 * Here are some important aspects to keep in mind about message order.
 * 1. When multiple SendRequest, Subscribe, and Publish calls are issued the
 *    corresponding messages will be dispatched to the broker in the same order
 *    that they are issued.
 * 2. The messages will be processed on the target service in the same order as
 *    well.
 */
class Service
{
public:
  Service(const std::string& name);
  ~Service();

  /**
   * Returns the name for the service.
   */
  const std::string GetName() const;

  /**
   * connect with the broker and do necessary handshake.
   */
  bool Connect(const std::string& brokerurl);

  /**
   * Ask the broker thread to terminate itself and all other services,
   * before stopping the application.
   */
  void TerminateAll() const;

  /**
   * Send a request to another service and get a reply. The reply is the
   * collection of message frames returned.
   */
  stlab::future<Message> SendRequest(const std::string& service, Message&& msg) const;

  /**
   * Same as `SendRequest`, however no reply is expected from the remote
   * service. On the receiving end, this is handled exactly as the `SendRequest`
   * call, except that any `SendReply` invocations in response to this sent
   * message are quietly ignored. This call will block till the message is on
   * the send queue (which is generally not a big issue, but worth mentioning).
   */
  void SendMessage(const std::string& service, Message&& msg) const;

  /**
   * Send a reply.
   */
  void SendReply(const Message& replyTo, Message&& reply) const;

  /**
   * Subscribe to a channel.
   */
  stlab::receiver<Message> Subscribe(const std::string& channel) const;

  /**
   * Send a notification message.
   */
  bool Publish(const std::string& channel, Message&& msg) const;

  /**
   * WIP: return active process stream i.e. the channel on which the service
   * receives messages to process.
   */
  stlab::receiver<Message> GetMainReceiver() const;

private:
  Service(const Service&) = delete;
  void operator=(const Service&) = delete;

  class SInternals;
  std::unique_ptr<SInternals> Internals;
};
}

#endif
