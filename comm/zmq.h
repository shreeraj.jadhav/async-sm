#ifndef comm_zmq_h
#define comm_zmq_h

#include <vector>
#include <zmq.hpp>

#include "comm.pb.h"
#include "message.h"

namespace zmq
{
class context_t;
class message_t;
class socket_t;
}

namespace google
{
namespace protobuf
{
class Message;
}
}

namespace comm
{

void ZMQInitialize(int io_threads = 1);
void ZMQFinalize();

/**
 * Returns the ZMQ context for the current process.
 * There's only 1 ZMQ context for the entire process.
 */
zmq::context_t& GetZMQContext();

/**
 * returns a complete message (including all frames)
 */
Message recv(zmq::socket_t& endpoint);

/**
 * send a message. If frames are empty, nothing is sent.
 */
bool send(Message&& frames, zmq::socket_t& endpoint, int flags = 0);

template <typename T>
bool z2p(const zmq::message_t& msg, T* t)
{
  comm::MessageWrapper wrapper;
  if (wrapper.ParseFromArray(msg.data(), msg.size()))
  {
    if (wrapper.content().Is<T>())
    {
      return wrapper.content().UnpackTo(t);
    }
  }
  return false;
}
}

#endif
