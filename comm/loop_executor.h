#ifndef comm_loop_executor_h
#define comm_loop_executor_h

#include <chrono>
#include <condition_variable>
#include <functional>
#include <list>
#include <map>
#include <mutex>
#include <atomic>
#include <stlab/concurrency/task.hpp>
#include <thread>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

namespace comm
{
namespace detail
{
struct task_queue
{
  // thread safe
  void create_thread_context(std::thread::id qid)
  {
    GuardLock lk(this->queue_mutex);
    this->queue[qid];
  }

  // thread safe
  template <typename F>
  void enqueue(std::thread::id qid, F&& f)
  {
    TCReference tref = this->get_thread_context(qid);
    if(tref.context)
    {
      tref.context->task_list.push_back(std::move(std::forward<F>(f)));
      LOG_S(9) << qid << ":enqueue::notify " << &tref.context->mutex;
      tref.unlock_and_notify();
    }
  }

  void mark_for_termination(std::thread::id qid)
  {
    TCReference tref = this->get_thread_context(qid);
    if(tref.context)
    {
      tref.context->terminate = true;
      // enqueue a dummy task to ensure that the thread wiggles out of wait condition
      tref.context->task_list.push_back([]() {});
      LOG_S(9) << qid << ":mark_for_termination::notify " << &tref.context->mutex;
      tref.unlock_and_notify();
    }
  }

  // thread safe
  template <typename F>
  void enqueue_on_idle(std::thread::id qid, F&& f)
  {
    TCReference tref = this->get_thread_context(qid);
    if(tref.context)
    {
      tref.context->idle_task_list.push_back(std::move(std::forward<F>(f)));
      LOG_S(9) << qid << ":enqueue_on_idle::notify " << &tref.context->mutex;
      tref.unlock_and_notify();
    }
  }

  // thread safe.
  void loop(std::thread::id qid, bool do_wait = true)
  {
    auto m_max = std::numeric_limits<int64_t>::max();
    int64_t max = m_max;
    LOG_S(9) << "start loop";
    TCReference tref = this->get_thread_context(qid);
    tref.unlock();
    if(tref.context == nullptr)
    {
      return;
    }
    do
    {
      if (tref.context->terminate)
      {
        LOG_S(9) << "terminate loop_executor.";
        break;
      }
      tref.lock();
      max = (max == m_max) ? static_cast<int64_t>(tref.context->task_list.size()) : max;
      if (tref.context->task_list.empty())
      {
        tref.unlock();
        this->do_idle_tasks(qid);
        tref.lock();
        if (do_wait)
        {
          const auto& q = tref.context->task_list;
          const auto& idle_q = tref.context->idle_task_list;
          LOG_S(9) << qid << "wait ";
          tref.context->cv.wait(tref.rlock(), [&] { return (!q.empty() || !idle_q.empty()); });
          LOG_S(9) << "waking up...";
          tref.unlock();
        }
        else
        {
          return;
        }
      }
      else
      {
        LOG_S(9) << "process";
        auto f = std::move(tref.context->task_list.front());
        tref.context->task_list.pop_front();
        tref.unlock();
        LOG_S(9) << "invoke";
        f();
        LOG_S(9) << "yield";
      }

      if (do_wait == false && --max == 0)
      {
        break;
      }
    } while (true);
  }

  void do_idle_tasks(std::thread::id qid)
  {
    TCReference tref = this->get_thread_context(qid);
    int64_t max = static_cast<int64_t>(tref.context->idle_task_list.size());
    tref.unlock();
    for (; max > 0; --max)
    {
      tref.lock();
      auto f = std::move(tref.context->idle_task_list.front());
      tref.context->idle_task_list.pop_front();
      tref.unlock();
      f();
    };
  }

  static task_queue& only_task_queue();

private:
  typedef std::unique_lock<std::mutex> UniqueLock;
  typedef std::lock_guard<std::mutex> GuardLock;

  struct ThreadContext
  {
  public:
    std::mutex mutex;
    std::condition_variable cv;
    std::atomic_bool terminate = {false};
    std::list<stlab::task<void()> > task_list;
    std::list<stlab::task<void()> > idle_task_list;
  };

  // This struct encapsulates the context pointer and mutex lock together.
  // One cannot access a context without a lock on its corresponding mutex.
  // The lock goes out of scope (thus unlocking the mutex)
  // along with the context pointer.
  struct TCReference
  {
  public:
    ThreadContext* context;
    TCReference(ThreadContext* c, UniqueLock&& lk)
    : context(c), lk(std::move(lk)) {}
    UniqueLock& rlock() { return lk; }
    void lock() { lk.lock(); }
    void unlock() { lk.unlock(); }
    void unlock_and_notify()
    {
      unlock();
      this->context->cv.notify_all();
    }
  private:
    UniqueLock lk;
  };

  // thread safe
  // This single function is responsible to acquire all the necessary locks
  // when a pointer to a thread context is requested.
  // The calling function is responsible for intermitent locking/unlocking
  // of the context level mutex, through the returned TCReference, if necessary.
  // Lock is automatically released when TCReference goes out of scope.
  TCReference get_thread_context(std::thread::id qid)
  {
    LOG_S(9) << qid << "get queue lock";
    GuardLock queue_lock(this->queue_mutex);
    LOG_S(9) << qid << "queue lock acquired";
    auto found = this->queue.find(qid);
    if (found != this->queue.end())
    {
      ThreadContext* ret = &(found->second);
      LOG_S(9) << qid << "get context lock" << &(ret->mutex);
      UniqueLock context_lock(ret->mutex);
      LOG_S(9) << qid << "context lock acquired" << &(ret->mutex);
      return TCReference(ret,std::move(context_lock));
    }
    else
    {
      LOG_S(WARNING) << "Cannot find thread context.";
      return TCReference(nullptr,UniqueLock());
    }
  }

  std::mutex queue_mutex;
  std::map<std::thread::id, ThreadContext> queue;

  static task_queue the_queue;
};

// see [1] for API inspiration
// [1] http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2014/n3970.html#executors.classes.loop
struct loop_executor_type
{
  using result_type = void;

  loop_executor_type()
    : creator_thread_id(std::this_thread::get_id())
  {
    task_queue::only_task_queue().create_thread_context(creator_thread_id);
  }

  template <typename F>
  void operator()(F&& f) const
  {
    // cout << std::this_thread::get_id() << ": enqueue" << endl;
    task_queue::only_task_queue().enqueue(this->creator_thread_id, std::move(f));
  }

  template <typename F>
  void enqueue_on_idle(F&& f) const
  {
    // cout << std::this_thread::get_id() << ": enqueue" << endl;
    task_queue::only_task_queue().enqueue_on_idle(this->creator_thread_id, std::move(f));
  }

  // runs any queues tasks, waiting for new ones, if queue is exhausted,
  // until `make_loop_exit()` is called.
  void loop() { task_queue::only_task_queue().loop(std::this_thread::get_id()); }

  void make_loop_exit(std::thread::id qid)
  {
    task_queue::only_task_queue().mark_for_termination(qid);
  }

  // same as loop, except quits when all tasks are exhausted.
  void run_queued_closures()
  {
    task_queue::only_task_queue().loop(std::this_thread::get_id(), false);
  }

  auto id() const { return this->creator_thread_id; }
private:
  std::thread::id creator_thread_id;
};

} // detail

// constexpr auto loop_executor = detail::loop_executor_type{};
using detail::loop_executor_type;

} // namespace comm
#endif
