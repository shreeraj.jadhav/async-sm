#ifndef comm_message_h
#define comm_message_h

#include <memory>
#include <zmq.hpp>

namespace google
{
namespace protobuf
{
class Message;
}
}

namespace comm
{

/**
 * @class Message
 * @brief A message (comprising of multiple frames).
 */
class Message
{
public:
  Message();
  ~Message();
  Message(Message&&) = default;
  Message(const Message&) = default;
  Message& operator=(const Message&) = delete;
  Message& operator=(Message&&) = default;

  //@{
  /**
   * Appends a message to the end.
   */
  void Append(zmq::message_t&& msg);
  void Append(const google::protobuf::Message& msg);
  void AppendEmptyFrame();
  void Append(Message&& msg);
  void AppendClone(const zmq::message_t& msg);
  void AppendClone(const Message& msg);
  //@}

  auto begin() { return this->Frames.begin(); }
  auto end() { return this->Frames.end(); }
  auto begin() const { return this->Frames.begin(); }
  auto end() const { return this->Frames.end(); }
  auto front() const { return this->Frames.front(); }
  auto front() { return this->Frames.front(); }
  auto back() const { return this->Frames.back(); }
  auto back() { return this->Frames.back(); }
  auto size() const { return this->Frames.size(); }
  void pop_back() { this->Frames.pop_back(); }
  void clear() { this->Frames.clear(); }
  auto at(size_t pos) { return this->Frames.at(pos); }
  auto at(size_t pos) const { return this->Frames.at(pos); }

  using iterator = typename std::vector<std::shared_ptr<zmq::message_t> >::iterator;
  using const_iterator = typename std::vector<std::shared_ptr<zmq::message_t> >::const_iterator;

  Message Clone() const { return this->Clone(this->begin(), this->end()); }
  static Message Clone(const_iterator b, const_iterator e);

  /**
   * Returns iterators for the envelope.
   */
  std::pair<const_iterator, const_iterator> GetEnvelope() const;
  std::pair<iterator, iterator> GetEnvelope();

  /**
   * Clears data frames i.e. all frames after first empty frame that marks the
   * end of the message envelope and beginning of the data frame.
   */
  void ClearData(bool remove_delimiter = false);

  void ClearEnvelope(bool remove_delimiter = false);

  auto GetSize() const { return this->Frames.size(); }

  std::string DebugString() const;

private:
  std::vector<std::shared_ptr<zmq::message_t> > Frames;
};
}

#endif
