#include "viewwidget.h"

#include <QVBoxLayout>
#include <QVTKOpenGLSimpleWidget.h>
#include <sm/client/renderviewproxy.h>

namespace app
{

using sm::client::RenderViewProxy;

//-----------------------------------------------------------------------------
ViewWidget::ViewWidget(std::shared_ptr<RenderViewProxy> rv, QWidget* p, Qt::WindowFlags f)
  : Superclass(p, f)
  , ViewProxy(rv)
{
  rv->UpdateVTKObjects();

  auto qvtkwidget = new QVTKOpenGLSimpleWidget(this);
  qvtkwidget->SetRenderWindow(rv->GetRenderWindow());
  rv->SetupInteractor(qvtkwidget->GetInteractor());

  auto l = new QVBoxLayout(this);
  l->addWidget(qvtkwidget);
  l->setSpacing(0);
  l->setMargin(0);
}

//-----------------------------------------------------------------------------
ViewWidget::~ViewWidget()
{
}
}
