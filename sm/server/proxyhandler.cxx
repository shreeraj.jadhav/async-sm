#include "proxyhandler.h"

#include "proxy.h"
#include "session.h"

#include <comm/loop_executor.h>
#include <comm/message.h>
#include <comm/service.h>
#include <comm/zmq.h>
#include <sm/common/proxy.pb.h>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/utility.hpp>

#include <chrono>

namespace sm
{
namespace server
{
class ProxyHandler::PHInternals
{
public:
  const Session& ParentSession;
  std::map<std::uint64_t, std::shared_ptr<Proxy> > Proxies;

  PHInternals(const Session& ref)
    : ParentSession(ref)
  {
  }

  comm::Message Process(const sm::common::ProxyState& pstate)
  {
    const auto gid = pstate.global_id();
    std::shared_ptr<Proxy> proxy;
    try
    {
      proxy = this->Proxies.at(gid);
    }
    catch (std::out_of_range)
    {
      proxy = Proxy::Create(pstate.xml_group(), pstate.xml_name(), this->ParentSession);
      assert(proxy);
      this->Proxies.emplace(std::make_pair(gid, proxy));
      proxy->SetGlobalID(gid);
    }
    proxy->UpdateState(pstate);
    return comm::Message();
  }

  comm::Message Process(const sm::common::ProxyGenericRequest& rstate)
  {
    comm::Message msg;
    const auto gid = rstate.global_id();
    try
    {
      auto proxy = this->Proxies.at(gid);
      msg.Append(proxy->HandleRequest(rstate));
    }
    catch (std::out_of_range)
    {
      abort();
    }
    return msg;
  }
};

//-----------------------------------------------------------------------------
ProxyHandler::ProxyHandler(const Session& session)
  : Internals(new ProxyHandler::PHInternals(session))
{
}

//-----------------------------------------------------------------------------
ProxyHandler::~ProxyHandler()
{
}

//-----------------------------------------------------------------------------
void ProxyHandler::Process(const comm::Message& msg)
{
  sm::common::ProxyState pstate;
  sm::common::ProxyGenericRequest pupr;
  if (msg.size() > 0)
  {
    if (comm::z2p(*msg.back(), &pupr))
    {
      this->GetSession().GetService().SendReply(msg, this->Internals->Process(pupr));
    }
    else if (comm::z2p(*msg.back(), &pstate))
    {
      this->GetSession().GetService().SendReply(msg, this->Internals->Process(pstate));
    }
  }
}

//-----------------------------------------------------------------------------
const Session& ProxyHandler::GetSession() const
{
  return this->Internals->ParentSession;
}

//-----------------------------------------------------------------------------
std::shared_ptr<Proxy> ProxyHandler::LocateProxy(uint64_t gid) const
{
  try
  {
    return this->Internals->Proxies.at(gid);
  }
  catch (std::out_of_range)
  {
    return nullptr;
  }
}

//-----------------------------------------------------------------------------
}
}
