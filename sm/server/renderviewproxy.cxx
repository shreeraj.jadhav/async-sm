#include "renderviewproxy.h"

#include "vtkextensions.h"
#include <vtkDataSetReader.h>
#include <vtkSmartPointer.h>

#include <comm/loop_executor.h>
#include <comm/message.h>
#include <comm/service.h>
#include <sm/server/proxyhandler.h>
#include <sm/server/session.h>
#include <zmq.hpp>

#include <sstream>
#include <utility>

#define LOGURU_WITH_STREAMS 1
#include <stlab/concurrency/default_executor.hpp>
#include <thirdparty/loguru/loguru.hpp>

namespace sm
{
namespace server
{

class RenderViewProxy::RVPInternals
{
public:
  std::uint64_t UpdatePipelineToken = 0;
};

//-----------------------------------------------------------------------------
RenderViewProxy::RenderViewProxy(const Session& session)
  : Proxy(vtkSmartPointer<vtkextensions::vtkPVRenderView>::New(), session)
  , Internals(new RenderViewProxy::RVPInternals())
{
}

//-----------------------------------------------------------------------------
RenderViewProxy::~RenderViewProxy()
{
}

//-----------------------------------------------------------------------------
void RenderViewProxy::SetGlobalID(uint64_t id)
{
  this->Superclass::SetGlobalID(id);
  auto view = vtkextensions::vtkPVRenderView::SafeDownCast(this->GetVTKObject());
  view->SetGlobalID(id);

  std::ostringstream channel;
  channel << "render." << id;
  this->Channel = channel.str();

  const auto& service = this->GetSession().GetService();
  if (service.GetName() == "rs") // subscribe only on rendering ranks.
  {
    // subscribe to channel to get new rendering geometries.
    auto executor = comm::loop_executor_type{};
    auto recvr = this->GetSession().GetService().Subscribe("renderingdata." + std::to_string(id));
    this->GeometryReceiver = recvr |
      [](const comm::Message& msg) {
        LOG_S(1) << "got raw data " << msg.DebugString();
        assert(msg.size() == 2);
        auto obj = vtkextensions::extract_vtk_object(*msg.at(0));
        vtkSmartPointer<vtkDataObject> dobj = vtkDataObject::SafeDownCast(obj);
        // FIXME: use protobuf
        uint64_t rid = *reinterpret_cast<uint64_t*>(msg.at(1)->data());
        return std::make_pair(rid, dobj);
      } |
      stlab::executor(executor) &
        [ view, &session = this->GetSession() ](
          const std::pair<uint64_t, vtkSmartPointer<vtkDataObject> > data_pair)
    {
      // do on main thread.
      LOG_S(1) << "got decoded data";
      auto rproxy = session.GetProxyHandler().LocateProxy(data_pair.first);
      if (rproxy)
      {
        if (auto r = vtkextensions::vtkGeometryRepresentation::SafeDownCast(rproxy->GetVTKObject()))
        {
          view->ProcessRenderingData(r, data_pair.second, session);
        }
      }
    };
    recvr.set_ready();

#if 0
    auto recvr = this->GetSession().GetService().Subscribe("renderingdata." + std::to_string(id));
    this->GeometryReceiver = recvr |
      [](const comm::Message& msg) {
        // do on subsciber thread.
        LOG_S(1) << "got raw data";
        assert(msg.size() == 2);
        auto reader = vtkSmartPointer<vtkDataSetReader>::New();
        reader->ReadFromInputStringOn();
        reader->SetBinaryInputString(
          reinterpret_cast<const char*>(msg.at(0)->data()), msg.at(0)->size());
        // FIXME: use protobuf
        uint64_t rid = *reinterpret_cast<uint64_t*>(msg.at(1)->data());
        return std::make_pair(rid, reader);
      } |
      stlab::executor(stlab::default_executor) &
        [](std::pair<uint64_t, vtkSmartPointer<vtkDataSetReader> > dpair) {
          // do on random thread.
          LOG_S(1) << "got raw data -- reading";
          dpair.second->Update();
          return std::make_pair(
            dpair.first, vtkSmartPointer<vtkDataObject>(dpair.second->GetOutputDataObject(0)));
        } |
      stlab::executor(executor) &
        [ view, &session = this->GetSession() ](
          const std::pair<uint64_t, vtkSmartPointer<vtkDataObject> > data_pair)
    {
      // do on main thread.
      LOG_S(1) << "got decoded data";
      auto rproxy = session.GetProxyHandler().LocateProxy(data_pair.first);
      if (rproxy)
      {
        if (auto r = vtkextensions::vtkGeometryRepresentation::SafeDownCast(rproxy->GetVTKObject()))
        {
          view->ProcessRenderingData(r, data_pair.second, session);
        }
      }
    };
    recvr.set_ready();
#endif
  }
}

//-----------------------------------------------------------------------------
void RenderViewProxy::UpdateState(const sm::common::ProxyState& pstate)
{
  this->Superclass::UpdateState(pstate);

  if (auto view = vtkextensions::vtkPVRenderView::SafeDownCast(this->GetVTKObject()))
  {
    for (const auto& pair : pstate.properties())
    {
      if (pair.first == "Representations")
      {
        std::vector<vtkSmartPointer<vtkextensions::vtkGeometryRepresentation> > reprs;
        for (const auto& value : pair.second.values())
        {
          if (value.value_case() == sm::common::Variant::kProxy)
          {
            auto rproxy = this->LocateProxy(value.proxy());
            if (auto r =
                  vtkextensions::vtkGeometryRepresentation::SafeDownCast(rproxy->GetVTKObject()))
            {
              reprs.push_back(r);
            }
          }
        }
        view->SetRepresentations(reprs);
      }
    }
  }
}

//-----------------------------------------------------------------------------
sm::common::ProxyGenericResponse RenderViewProxy::HandleRequest(
  const sm::common::ProxyGenericRequest& rstate)
{
  if (rstate.body().Is<sm::common::UpdatePipeline>())
  {
    LOG_S(INFO) << "RenderViewProxy::HandleRequest::UpdatePipeline";
    if (auto view = vtkextensions::vtkPVRenderView::SafeDownCast(this->GetVTKObject()))
    {
      sm::common::UpdatePipeline up;
      rstate.body().UnpackTo(&up);
      auto token = this->Internals->UpdatePipelineToken++;

      // we want to collapse update requests, lets us to that.
      // todo: we can add "no later than" support to this as well, that
      // way the task won't get pushed back infinitely.
      auto f = [this, token, up, view]() {
        if (this->Internals->UpdatePipelineToken == (token + 1))
        {
          LOG_S(INFO) << "do view update";
          view->Update(up, this->GetSession(), this->Channel);
        }
        else
        {
          LOG_S(INFO) << "skip view update";
        }
      };

      auto executor = comm::loop_executor_type{};
      executor.enqueue_on_idle(f);
    }
    return sm::common::ProxyGenericResponse();
  }
  else if (rstate.body().Is<sm::common::Render>())
  {
    // LOG_S(INFO) << "RenderViewProxy::HandleRequest::Render";
    sm::common::Render render;
    rstate.body().UnpackTo(&render);
    // todo: how to collapse multiple renders?
    this->Render(render);
    return sm::common::ProxyGenericResponse();
  }
  else if (rstate.body().Is<sm::common::Variant>())
  {
    sm::common::Variant var;
    rstate.body().UnpackTo(&var);
    if (var.value_case() == sm::common::Variant::kStr)
    {
      if (var.str() == "GetPropBounds")
      {
        if (auto view = vtkextensions::vtkPVRenderView::SafeDownCast(this->GetVTKObject()))
        {
          auto pbds = view->GetPropBounds();
          sm::common::Bounds bds;
          bds.set_xmin(pbds[0]);
          bds.set_xmax(pbds[1]);
          bds.set_ymin(pbds[2]);
          bds.set_ymax(pbds[3]);
          bds.set_zmin(pbds[4]);
          bds.set_zmax(pbds[5]);

          sm::common::ProxyGenericResponse response;
          response.mutable_body()->PackFrom(bds);
          return response;
        }
      }
    }
    LOG_S(ERROR) << "Unhandled command: " << rstate.ShortDebugString();
    return sm::common::ProxyGenericResponse();
  }
  else
  {
    return this->Superclass::HandleRequest(rstate);
  }
}

//-----------------------------------------------------------------------------
void RenderViewProxy::Render(const sm::common::Render& renderReq)
{
  if (auto view = vtkextensions::vtkPVRenderView::SafeDownCast(this->GetVTKObject()))
  {
    view->Render(renderReq, this->GetSession(), this->Channel);
  }
}
}
}
