#ifndef sm_server_renderviewproxy_h
#define sm_server_renderviewproxy_h

#include "proxy.h"
#include <stlab/concurrency/channel.hpp>

#include <memory>

namespace sm
{
namespace server
{

class RenderViewProxy : public Proxy
{
  using Superclass = Proxy;

public:
  RenderViewProxy(const Session& session);
  ~RenderViewProxy();

  void SetGlobalID(uint64_t id) override;
  sm::common::ProxyGenericResponse HandleRequest(
    const sm::common::ProxyGenericRequest& rstate) override;
  void UpdateState(const sm::common::ProxyState& pstate) override;

protected:
  void Render(const sm::common::Render& render);

private:
  RenderViewProxy(const RenderViewProxy&) = delete;
  void operator=(const RenderViewProxy&) = delete;
  std::string Channel;
  stlab::receiver<void> GeometryReceiver;

  class RVPInternals;
  std::unique_ptr<RVPInternals> Internals;
};
}
}

#endif
