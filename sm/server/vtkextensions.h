#ifndef sm_server_vtkextensions_h
#define sm_server_vtkextensions_h

#include <sm/server/session.h>
#include <vtkDataObjectAlgorithm.h>
#include <vtkNew.h>
#include <vtkSmartPointer.h>
#include <vtkVector.h>
#include <vtkWeakPointer.h>

#include <vector>

class vtkActor;
class vtkCompositePolyDataMapper2;
class vtkDataSetWriter;
class vtkGeometryFilter;
class vtkInformationObjectBaseKey;
class vtkInformationRequestKey;
class vtkOutlineFilter;
class vtkRenderer;
class vtkRenderWindow;
class vtkTextActor;
class vtkWindowToImageFilter;

namespace sm
{
namespace common
{
class Render;
class UpdatePipeline;
}
namespace server
{
namespace vtkextensions
{
//@{
/**
 * these currently pack raw pointers.
 * we need to add mechanism to `comm::send()` to use data writers/readers
 * when shipping messages with heavy data to `tcp://` sockets.
 */
zmq::message_t pack_vtk_object(vtkSmartPointer<vtkObject> obj);
vtkSmartPointer<vtkObject> extract_vtk_object(const zmq::message_t& msg);
//@}

class vtkGeometryRepresentation;

//==============================================================================
class vtkPVRenderView : public vtkObject
{
public:
  static vtkPVRenderView* New();
  vtkTypeMacro(vtkPVRenderView, vtkObject);

  /// all views/representations get unique ids for the session across
  /// all services.
  vtkSetMacro(GlobalID, uint64_t);
  vtkGetMacro(GlobalID, uint64_t);

  static vtkInformationRequestKey* REQUEST_DATA();
  static vtkInformationRequestKey* REQUEST_RENDER();
  static vtkInformationObjectBaseKey* VIEW();

  static void PublishDataForRendering(
    vtkInformation* request, vtkGeometryRepresentation* repr, vtkSmartPointer<vtkDataObject> data);

  vtkVector<double, 6> GetPropBounds();

  void Render(
    const sm::common::Render& req, const sm::server::Session& session, const std::string& channel);

  void Update(const sm::common::UpdatePipeline& req, const sm::server::Session& session,
    const std::string& channel);

  void ProcessRenderingData(vtkGeometryRepresentation* repr, vtkSmartPointer<vtkDataObject> data,
    const sm::server::Session& session);

  void SetSize(int dx, int dy);

  void SetRepresentations(const std::vector<vtkSmartPointer<vtkGeometryRepresentation> >& reprs);

  vtkRenderer* GetRenderer();

protected:
  vtkPVRenderView();
  virtual ~vtkPVRenderView() = default;

  void Render(const sm::server::Session& session, bool fromasync = false);

private:
  vtkPVRenderView(const vtkPVRenderView&) = delete;
  void operator=(const vtkPVRenderView&) = delete;

  uint64_t GlobalID = 0;
  vtkNew<vtkRenderWindow> Window;
  vtkNew<vtkRenderer> Renderer;
  vtkNew<vtkWindowToImageFilter> W2IFilter;
  vtkNew<vtkDataSetWriter> Writer;

  bool RenderQueued = false;

  const sm::server::Session* CurrentSession = nullptr;

  std::vector<vtkSmartPointer<vtkGeometryRepresentation> > Representations;
};

//==============================================================================

class vtkGeometryRepresentation : public vtkDataObjectAlgorithm
{
public:
  static vtkGeometryRepresentation* New();
  vtkTypeMacro(vtkGeometryRepresentation, vtkDataObjectAlgorithm);

  /// all views/representations get unique ids for the session across
  /// all services.
  vtkSetMacro(GlobalID, uint64_t);
  vtkGetMacro(GlobalID, uint64_t);

  int ProcessRequest(
    vtkInformation* request, vtkInformationVector** inInfo, vtkInformationVector* outInfo) override;

  /// Need to provide view with information about when the pipeline is updated
  vtkMTimeType GetPipelineDataTime() { return this->PipelineDataTime; }

  void AddToView(vtkPVRenderView* view);

  /// show time annotation
  void ShowTimeAnnotation(bool);

protected:
  vtkGeometryRepresentation();
  virtual ~vtkGeometryRepresentation() = default;

  int FillInputPortInformation(int port, vtkInformation* info) override;
  int RequestData(
    vtkInformation*, vtkInformationVector** inputVector, vtkInformationVector*) override;

private:
  vtkGeometryRepresentation(const vtkGeometryRepresentation&) = delete;
  void operator=(const vtkGeometryRepresentation&) = delete;

  uint64_t GlobalID = 0;
  vtkNew<vtkGeometryFilter> GeometryFilter;
  vtkNew<vtkOutlineFilter> OutlineFilter;
  vtkSmartPointer<vtkDataObject> GeometryToDeliver;
  vtkSmartPointer<vtkDataObject> GeometryToRender;
  vtkNew<vtkCompositePolyDataMapper2> Mapper;
  vtkNew<vtkActor> Actor;
  vtkTimeStamp PipelineDataTime;

  vtkWeakPointer<vtkPVRenderView> View;

  // adding ability to show time label to a representation showing outline
  vtkNew<vtkTextActor> TextActor;
};

//==============================================================================
}
}
}

#endif
