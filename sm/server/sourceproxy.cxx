#include "sourceproxy.h"

#include <vtkAlgorithm.h>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>
namespace sm
{
namespace server
{
//-----------------------------------------------------------------------------
SourceProxy::SourceProxy(vtkAlgorithm* obj, const Session& session)
  : Superclass(obj, session)
{
}

//-----------------------------------------------------------------------------
SourceProxy::~SourceProxy()
{
}

//-----------------------------------------------------------------------------
vtkAlgorithm* SourceProxy::GetVTKAlgorithm() const
{
  return vtkAlgorithm::SafeDownCast(this->GetVTKObject());
}

//-----------------------------------------------------------------------------
void SourceProxy::UpdateState(const sm::common::ProxyState& pstate)
{
  this->Superclass::UpdateState(pstate);
  // TODO: only update things that change.
  if (auto algo = this->GetVTKAlgorithm())
  {
    for (const auto& pair : pstate.properties())
    {
      if (pair.first == "Input" && pair.second.values().size() == 1 &&
        pair.second.values(0).value_case() == sm::common::Variant::kProxy)
      {
        auto pid = pair.second.values(0).proxy();
        if (auto iproxy = this->LocateProxy(pid))
        {
          auto ialgo = vtkAlgorithm::SafeDownCast(iproxy->GetVTKObject());
          algo->SetInputConnection(ialgo->GetOutputPort());
          LOG_S(INFO) << "setting input: " << ialgo->GetClassName() << " (" << ialgo << ") --> "
                      << algo->GetClassName() << " (" << algo << ")";
        }
      }
    }
  }
}

//-----------------------------------------------------------------------------
sm::common::ProxyGenericResponse SourceProxy::HandleRequest(
  const sm::common::ProxyGenericRequest& rstate)
{
  if (rstate.body().Is<sm::common::UpdatePipeline>())
  {
    sm::common::UpdatePipeline up;
    rstate.body().UnpackTo(&up);
    this->UpdatePipeline(up.time());
    return sm::common::ProxyGenericResponse();
  }
  else
  {
    return this->Superclass::HandleRequest(rstate);
  }
}

//-----------------------------------------------------------------------------
void SourceProxy::UpdatePipeline(double time)
{
  if (auto algo = this->GetVTKAlgorithm())
  {
    algo->Update();
  }
}
}
}
