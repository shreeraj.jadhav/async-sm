#include "proxy.h"

#include <vtkAlgorithm.h>
#include <vtkCommand.h>
#include <vtkCompositeDataIterator.h>
#include <vtkCompositeDataSet.h>
#include <vtkNew.h>
#include <vtkObject.h>
#include <vtkSmartPointer.h>

#include <sm/server/session.h>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include "contourproxy.h"
#include "geometryrepresentationproxy.h"
#include "liveproxy.h"
#include "proxyhandler.h"
#include "renderviewproxy.h"
#include "sliceproxy.h"
#include "spheresourceproxy.h"
#include "waveletproxy.h"

#if defined(ENABLE_SENSEI)
#include "senseiliveproxy.h"
#endif

#include <chrono>
#include <iomanip>
#include <locale>
#include <sstream>
#include <thread>

namespace sm
{
namespace server
{

//=============================================================================
std::shared_ptr<Proxy> Proxy::Create(
  const std::string& xmlgroup, const std::string& xmlname, const Session& session)
{
  if (xmlname == "SphereSource")
  {
    LOG_S(INFO) << "Creating SphereSource";
    return std::make_shared<SphereSourceProxy>(session);
  }
  else if (xmlname == "RenderView")
  {
    LOG_S(INFO) << "Creating RenderView";
    return std::make_shared<RenderViewProxy>(session);
  }
  else if (xmlname == "GeometryRepresentation")
  {
    LOG_S(INFO) << "Creating GeometryRepresentation";
    return std::make_shared<GeometryRepresentationProxy>(session);
  }
  else if (xmlname == "Wavelet")
  {
    LOG_S(INFO) << "Creating Wavelet";
    return std::make_shared<WaveletProxy>(session);
  }
  else if (xmlname == "Contour")
  {
    LOG_S(INFO) << "Creating Contour";
    return std::make_shared<ContourProxy>(session);
  }
  else if (xmlname == "Live")
  {
    LOG_S(INFO) << "Creating Live";
    return std::make_shared<LiveProxy>(session);
  }
  else if (xmlname == "SenseiLive")
  {
#if defined(ENABLE_SENSEI)
    LOG_S(INFO) << "Creating SENSEI Live";
    return std::make_shared<SenseiLiveProxy>(session);
#else
    LOG_S(ERROR) << "Not built with SENSEI support. Aborting for debugging.";
    abort();
#endif
  }
  else if (xmlname == "Slice")
  {
    LOG_S(INFO) << "Creating Slice";
    return std::make_shared<SliceProxy>(session);
  }
  abort();
}

namespace detail
{
vtkIdType elementCount(vtkDataObject* dobj, int type)
{
  if (auto cd = vtkCompositeDataSet::SafeDownCast(dobj))
  {
    vtkIdType count{ 0 };
    auto iter = cd->NewIterator();
    for (iter->InitTraversal(); !iter->IsDoneWithTraversal(); iter->GoToNextItem())
    {
      count += elementCount(iter->GetCurrentDataObject(), type);
    }
    iter->Delete();
    return count;
  }
  else
  {
    return dobj->GetNumberOfElements(type);
  }
}

std::string dataInformation(vtkObject* obj)
{
  if (auto algo = vtkAlgorithm::SafeDownCast(obj))
  {
    if (auto dobj = algo->GetOutputDataObject(0))
    {
      std::ostringstream str;
      str.imbue(std::locale(""));
      str << "<p><b>data type</b>: " << dobj->GetClassName() << "</p>";
      str << "<p><b>number of cells</b>: " << std::fixed << elementCount(dobj, vtkDataObject::CELL)
          << "</p>";
      str << "<p><b>number of points</b>: " << std::fixed
          << elementCount(dobj, vtkDataObject::POINT) << "</p>";
      return str.str();
    }
  }
  return "";
}
}

//=============================================================================
class Proxy::PInternals
{
  unsigned long ProgressObserverId;
  unsigned long EndEventObserverId;

public:
  vtkSmartPointer<vtkObject> VTKObject;
  const Session& ParentSession;
  sm::common::ProxyState ProxyState;

  PInternals(vtkObject* obj, const Session& session)
    : ProgressObserverId(0)
    , EndEventObserverId(0)
    , VTKObject(obj)
    , ParentSession(session)
  {
    if (obj)
    {
      this->ProgressObserverId =
        obj->AddObserver(vtkCommand::ProgressEvent, this, &PInternals::HandleProgressEvent);
      this->EndEventObserverId =
        obj->AddObserver(vtkCommand::EndEvent, this, &PInternals::HandleEndEvent);
    }
  }

  ~PInternals()
  {
    if (this->ProgressObserverId > 0 && this->VTKObject)
    {
      this->VTKObject->RemoveObserver(this->ProgressObserverId);
      this->ProgressObserverId = 0;
    }
  }

private:
  void HandleProgressEvent(vtkObject* obj, unsigned long, void* vdata)
  {
    double amount = *reinterpret_cast<double*>(vdata);
    amount = std::max(0.0, amount);
    amount = std::min(1.0, amount);

    // TODO: need to add a mechanism to throttle on send.
    sm::common::ProxyProgress pg;
    pg.set_global_id(this->ProxyState.global_id());
    pg.set_text(obj->GetClassName());
    pg.set_value(amount);

    comm::Message msg;
    msg.Append(pg);

    this->ParentSession.GetService().Publish("progress", std::move(msg));
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(1ms);
  }

  void HandleEndEvent(vtkObject*, unsigned long, void*)
  {
    // TODO: need to add a mechanism to throttle on send.
    sm::common::ProxyPipelineUpdated pg;
    pg.set_global_id(this->ProxyState.global_id());

    comm::Message msg;
    msg.Append(pg);

    this->ParentSession.GetService().Publish("pipeline-updated", std::move(msg));
  }
};

//-----------------------------------------------------------------------------
Proxy::Proxy(vtkObject* obj, const Session& session)
  : Internals(new Proxy::PInternals(obj, session))
{
}

//-----------------------------------------------------------------------------
Proxy::~Proxy()
{
}

//-----------------------------------------------------------------------------
const Session& Proxy::GetSession() const
{
  return this->Internals->ParentSession;
}

//-----------------------------------------------------------------------------
vtkObject* Proxy::GetVTKObject() const
{
  return this->Internals->VTKObject;
}

//-----------------------------------------------------------------------------
void Proxy::UpdateState(const sm::common::ProxyState& pstate)
{
  this->Internals->ProxyState = pstate;
}

//-----------------------------------------------------------------------------
sm::common::ProxyGenericResponse Proxy::HandleRequest(
  const sm::common::ProxyGenericRequest& grequest)
{
  sm::common::ProxyGenericResponse response;
  if (grequest.body().Is<sm::common::GatherInformation>())
  {
    sm::common::GatherInformation ginfo;
    grequest.body().UnpackTo(&ginfo);
    if (ginfo.type() == "DataInformation")
    {
      sm::common::DataInformation dinfo;
      if (auto vtkobject = this->GetVTKObject())
      {
        std::ostringstream str;
        str << "<html><body>"
            << "<p><b>class</b>: " << vtkobject->GetClassName() << "</p>"
            << detail::dataInformation(vtkobject) << "</body></html>";
        dinfo.set_text(str.str());
      }
      response.mutable_body()->PackFrom(dinfo);
    }
  }
  return response;
}

//-----------------------------------------------------------------------------
std::shared_ptr<Proxy> Proxy::LocateProxy(uint64_t gid)
{
  return this->GetSession().GetProxyHandler().LocateProxy(gid);
}
}
}
