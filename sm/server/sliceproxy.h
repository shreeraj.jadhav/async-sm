#ifndef sm_server_sliceproxy_h
#define sm_server_sliceproxy_h

#include "sourceproxy.h"

namespace sm
{
namespace server
{

class SliceProxy : public SourceProxy
{
public:
  SliceProxy(const Session& session);
  ~SliceProxy() override;

  void UpdateState(const sm::common::ProxyState& pstate) override;

private:
  SliceProxy(const SliceProxy&) = delete;
  void operator=(const SliceProxy&) = delete;
};
}
}

#endif
