#include "geometryrepresentationproxy.h"
#include "vtkextensions.h"

#include <vtkSmartPointer.h>

namespace sm
{
namespace server
{

//-----------------------------------------------------------------------------
GeometryRepresentationProxy::GeometryRepresentationProxy(const Session& session)
  : Superclass(vtkSmartPointer<vtkextensions::vtkGeometryRepresentation>::New(), session)
{
}

//-----------------------------------------------------------------------------
GeometryRepresentationProxy::~GeometryRepresentationProxy()
{
}

//-----------------------------------------------------------------------------
void GeometryRepresentationProxy::SetGlobalID(uint64_t id)
{
  this->Superclass::SetGlobalID(id);
  vtkextensions::vtkGeometryRepresentation::SafeDownCast(this->GetVTKObject())->SetGlobalID(id);
}

//-----------------------------------------------------------------------------
void GeometryRepresentationProxy::UpdateState(const sm::common::ProxyState& pstate)
{
  this->Superclass::UpdateState(pstate);
  if (auto repr = vtkextensions::vtkGeometryRepresentation::SafeDownCast(this->GetVTKObject()))
  {
    for (const auto& pair : pstate.properties())
    {
      if (pair.first == "ShowTimeAnnotation" && pair.second.values().size() == 1 &&
        pair.second.values(0).value_case() == sm::common::Variant::kInteger)
      {
        repr->ShowTimeAnnotation(pair.second.values(0).integer() != 0);
      }
    }
  }
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
}
}
