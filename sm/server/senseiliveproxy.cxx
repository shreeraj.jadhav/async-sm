#include "senseiliveproxy.h"

#include "session.h"

#include <vtkCellDataToPointData.h>
#include <vtkCompositeDataIterator.h>
#include <vtkDataSet.h>
#include <vtkDoubleArray.h>
#include <vtkFieldData.h>
#include <vtkIntArray.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkNew.h>
#include <vtkPointData.h>
#include <vtkSmartPointer.h>
#include <vtkTrivialProducer.h>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <comm/loop_executor.h>
#include <stlab/concurrency/default_executor.hpp>
#include <stlab/concurrency/utility.hpp>

#include <future>
#include <mpi.h>
#include <thread>

#include "LIVEDataAdaptor.h"

namespace sm
{
namespace server
{

namespace detail
{
// just to make our lives easier for now, mark scalars in point data.
void markScalars(vtkDataObject* dobj)
{
  if (auto mb = vtkCompositeDataSet::SafeDownCast(dobj))
  {
    auto iter = mb->NewIterator();
    for (iter->InitTraversal(); !iter->IsDoneWithTraversal(); iter->GoToNextItem())
    {
      markScalars(iter->GetCurrentDataObject());
    }
    iter->Delete();
  }
  else if (auto ds = vtkDataSet::SafeDownCast(dobj))
  {
    ds->GetPointData()->SetScalars(ds->GetPointData()->GetArray(0));
  }
}

static void addTime(vtkDataObject* dobj, long timestep, double time)
{
  vtkNew<vtkIntArray> ts;
  ts->SetName("timestep");
  ts->SetNumberOfTuples(1);
  ts->SetTypedComponent(0, 0, static_cast<int>(timestep));
  dobj->GetFieldData()->AddArray(ts);

  vtkNew<vtkDoubleArray> t;
  t->SetName("time");
  t->SetNumberOfTuples(1);
  t->SetTypedComponent(0, 0, time);
  dobj->GetFieldData()->AddArray(t);
}
}

class SenseiLiveProxy::LInternals
{
  const sm::server::Session& Session;

  std::atomic_int Done;
  std::future<bool> ReturnCode;
  stlab::receiver<void> Receiver;
  std::string Filename;

public:
  LInternals(const sm::server::Session& s)
    : Session(s)
    , Done(0)
  {
  }

  ~LInternals() { this->Done++; }

  bool StartIfNeeded(std::string fname, vtkTrivialProducer* producer)
  {
    if (!fname.empty() && this->ReturnCode.valid() == false)
    {
      stlab::receiver<vtkSmartPointer<vtkDataObject> > recvr;
      stlab::sender<vtkSmartPointer<vtkDataObject> > sender;
      std::tie(sender, recvr) =
        stlab::channel<vtkSmartPointer<vtkDataObject> >(stlab::immediate_executor);
      auto f = [& session = this->Session, &done = this->Done, fname ](
        stlab::sender<vtkSmartPointer<vtkDataObject> > && send)
      {

        auto dataAdaptor = vtkSmartPointer<sensei::LIVEDataAdaptor>::New();
        dataAdaptor->SetCommunicator(MPI_COMM_WORLD);
        if (dataAdaptor->Open(fname.c_str()) != EXIT_SUCCESS)
        {
          LOG_S(ERROR) << "failed!!!";
          return false;
        }

        do
        {
          long timeStep = dataAdaptor->GetDataTimeStep();
          double time = dataAdaptor->GetDataTime();

          LOG_S(INFO) << "Processing time step " << timeStep << " time " << time;

          std::vector<std::string> meshnames;
          if (dataAdaptor->GetMeshNames(meshnames) != EXIT_SUCCESS)
          {
            LOG_S(ERROR) << "failed!!!";
            return false;
          }

          vtkDataObject* dobj = nullptr;
          dataAdaptor->GetCompleteMesh(meshnames.front(), false, dobj);
          // hack: dataAdaptor get arrayanmes is broken for LIVE.
          // dataAdaptor->AddArray(dobj, meshnames.front(), vtkDataObject::CELL, "density0");
          if (dobj)
          {
            vtkNew<vtkCellDataToPointData> c2p;
            c2p->SetInputDataObject(dobj);
            c2p->Update();

            auto data = vtkSmartPointer<vtkDataObject>(c2p->GetOutputDataObject(0));
            detail::markScalars(data);
            detail::addTime(data, timeStep, time);
            send(data);

            // LOG_S(INFO) << "new live data available " << clone->GetClassName();
            session.GetService().Publish("liveupdate", comm::Message());
            dobj->Delete();
          }
          // let the data adaptor release the mesh and data from this
          // time step
          dataAdaptor->ReleaseData();
          // std::this_thread::sleep_for(std::chrono::milliseconds(100));
        } while (!done && dataAdaptor->Advance() == EXIT_SUCCESS);
        LOG_S(INFO) << "read completed.";
        return true;
      };

      auto executor = comm::loop_executor_type{};
      this->Receiver = recvr |
        stlab::executor(executor) &
          [producer](vtkSmartPointer<vtkDataObject> dobj) { producer->SetOutput(dobj); };
      recvr.set_ready();

      this->Filename = fname;
      this->ReturnCode = std::async(std::launch::async, f, std::move(sender));
    }
    else if (fname != this->Filename)
    {
      LOG_S(ERROR) << "changing filename not supported.";
    }
    return false;
  }
};

//-----------------------------------------------------------------------------
SenseiLiveProxy::SenseiLiveProxy(const Session& session)
  : SourceProxy(vtkSmartPointer<vtkTrivialProducer>::New(), session)
  , Internals(new SenseiLiveProxy::LInternals(session))
{
  auto tp = vtkTrivialProducer::SafeDownCast(this->GetVTKObject());
  tp->SetOutput(vtkSmartPointer<vtkMultiBlockDataSet>::New());

  int inited = 0;
  MPI_Initialized(&inited);
  if (!inited)
  {
    LOG_S(ERROR) << "SenseiLiveProxy requires MPI. Aborting for debugging purposes.";
    abort();
  }
}

//-----------------------------------------------------------------------------
SenseiLiveProxy::~SenseiLiveProxy()
{
}

//-----------------------------------------------------------------------------
void SenseiLiveProxy::UpdateState(const sm::common::ProxyState& pstate)
{
  this->SourceProxy::UpdateState(pstate);
  if (auto tp = vtkTrivialProducer::SafeDownCast(this->GetVTKObject()))
  {
    std::string fname;
    for (const auto& pair : pstate.properties())
    {
      if (pair.first == "Filename" && pair.second.values().size() == 1 &&
        pair.second.values(0).value_case() == sm::common::Variant::kStr)
      {
        fname = pair.second.values(0).str();
      }
    }
    this->Internals->StartIfNeeded(fname, tp);
  }
}
}
}
