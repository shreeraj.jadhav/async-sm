#ifndef sm_server_sourceproxy_h
#define sm_server_sourceproxy_h

#include "proxy.h"

class vtkAlgorithm;
namespace sm
{
namespace server
{
class SourceProxy : public Proxy
{
  using Superclass = Proxy;

public:
  SourceProxy(vtkAlgorithm* obj, const Session& session);
  ~SourceProxy();

  vtkAlgorithm* GetVTKAlgorithm() const;

  sm::common::ProxyGenericResponse HandleRequest(
    const sm::common::ProxyGenericRequest& rstate) override;
  void UpdateState(const sm::common::ProxyState& pstate) override;

protected:
  void UpdatePipeline(double time);

private:
  SourceProxy(const SourceProxy&) = delete;
  void operator=(const SourceProxy&) = delete;
};
}
}

#endif
