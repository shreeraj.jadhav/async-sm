#include "session.h"

#include <comm/loop_executor.h>
#include <comm/zmq.h>
#include <sm/common/proxy.pb.h>
#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/utility.hpp>

#include "proxyhandler.h"

namespace sm
{
namespace server
{

class Session::SInternals
{
public:
  stlab::receiver<comm::Message> MainReceiver;
  std::vector<stlab::receiver<void> > Holder;
  ProxyHandler PHandler;

  SInternals(Session* self)
    : PHandler(*self)
  {
  }
};

//-----------------------------------------------------------------------------
Session::Session()
  : Internals(new Session::SInternals(this))
{
}

//-----------------------------------------------------------------------------
Session::~Session()
{
  this->Internals.reset();
  this->Service = nullptr;
}

//-----------------------------------------------------------------------------
bool Session::Initialize(const std::string& brokerURL, const std::string& name)
{
  this->Service = std::make_shared<comm::Service>(name);
  if (!this->Service->Connect(brokerURL))
  {
    this->Service = nullptr;
    return false;
  }

  // auto& service = this->Service;
  auto& internals = (*this->Internals);

  auto loop_executor = comm::loop_executor_type{};
  internals.MainReceiver = this->Service->GetMainReceiver();

  using namespace std::placeholders;
  auto hold = internals.MainReceiver |
    stlab::executor(loop_executor) & std::bind(&ProxyHandler::Process, &internals.PHandler, _1);
  internals.Holder.push_back(std::move(hold));
  internals.MainReceiver.set_ready();
  return true;
}

//-----------------------------------------------------------------------------
const comm::Service& Session::GetService() const
{
  return *this->Service.get();
}

//-----------------------------------------------------------------------------
const ProxyHandler& Session::GetProxyHandler() const
{
  return this->Internals->PHandler;
}
}
}
