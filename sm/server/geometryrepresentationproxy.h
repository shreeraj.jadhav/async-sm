#ifndef sm_server_geometryrepresentationproxy_h
#define sm_server_geometryrepresentationproxy_h

#include "sourceproxy.h"

namespace sm
{
namespace server
{

class GeometryRepresentationProxy : public SourceProxy
{
  using Superclass = SourceProxy;

public:
  GeometryRepresentationProxy(const Session& session);
  ~GeometryRepresentationProxy();

  void SetGlobalID(uint64_t id) override;
  void UpdateState(const sm::common::ProxyState& pstate) override;

private:
  GeometryRepresentationProxy(const GeometryRepresentationProxy&) = delete;
  void operator=(const GeometryRepresentationProxy&) = delete;
};
}
}

#endif
