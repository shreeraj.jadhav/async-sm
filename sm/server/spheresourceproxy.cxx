#include "spheresourceproxy.h"

#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

namespace sm
{
namespace server
{

//-----------------------------------------------------------------------------
SphereSourceProxy::SphereSourceProxy(const Session& session)
  : SourceProxy(vtkSmartPointer<vtkSphereSource>::New(), session)
{
}

//-----------------------------------------------------------------------------
SphereSourceProxy::~SphereSourceProxy()
{
}

//-----------------------------------------------------------------------------
void SphereSourceProxy::UpdateState(const sm::common::ProxyState& pstate)
{
  this->SourceProxy::UpdateState(pstate);
  if (auto sphere = vtkSphereSource::SafeDownCast(this->GetVTKObject()))
  {
    for (const auto& pair : pstate.properties())
    {
      if (pair.first == "Center" && pair.second.values().size() == 3 &&
        pair.second.values(0).value_case() == sm::common::Variant::kFloat64 &&
        pair.second.values(1).value_case() == sm::common::Variant::kFloat64 &&
        pair.second.values(2).value_case() == sm::common::Variant::kFloat64)
      {
        sphere->SetCenter(pair.second.values(0).float64(), pair.second.values(1).float64(),
          pair.second.values(2).float64());
        LOG_S(INFO) << "Sphere::SetCenter: " << sphere->GetCenter()[0] << ", "
                    << sphere->GetCenter()[1] << ", " << sphere->GetCenter()[2];
      }
      else if (pair.first == "Radius" && pair.second.values().size() == 1 &&
        pair.second.values(0).value_case() == sm::common::Variant::kFloat64)
      {
        sphere->SetRadius(pair.second.values(0).float64());
      }
      else if (pair.first == "ThetaResolution" && pair.second.values().size() == 1 &&
        pair.second.values(0).value_case() == sm::common::Variant::kInteger)
      {
        sphere->SetThetaResolution(pair.second.values(0).integer());
      }
      else if (pair.first == "StartTheta" && pair.second.values().size() == 1 &&
        pair.second.values(0).value_case() == sm::common::Variant::kFloat64)
      {
        sphere->SetStartTheta(pair.second.values(0).float64());
      }
      else if (pair.first == "EndTheta" && pair.second.values().size() == 1 &&
        pair.second.values(0).value_case() == sm::common::Variant::kFloat64)
      {
        sphere->SetEndTheta(pair.second.values(0).float64());
      }
      else if (pair.first == "PhiResolution" && pair.second.values().size() == 1 &&
        pair.second.values(0).value_case() == sm::common::Variant::kInteger)
      {
        sphere->SetPhiResolution(pair.second.values(0).integer());
      }
      else if (pair.first == "StartPhi" && pair.second.values().size() == 1 &&
        pair.second.values(0).value_case() == sm::common::Variant::kFloat64)
      {
        sphere->SetStartPhi(pair.second.values(0).float64());
      }
      else if (pair.first == "EndPhi" && pair.second.values().size() == 1 &&
        pair.second.values(0).value_case() == sm::common::Variant::kFloat64)
      {
        sphere->SetEndPhi(pair.second.values(0).float64());
      }
    }
  }
}
}
}
