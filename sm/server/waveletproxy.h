#ifndef sm_server_waveletproxy_h
#define sm_server_waveletproxy_h

#include "sourceproxy.h"

namespace sm
{
namespace server
{
class WaveletProxy : public SourceProxy
{
public:
  WaveletProxy(const Session& session);
  ~WaveletProxy();

  void UpdateState(const sm::common::ProxyState& pstate) override;

private:
  WaveletProxy(const WaveletProxy&) = delete;
  void operator=(const WaveletProxy&) = delete;
};
}
}

#endif
