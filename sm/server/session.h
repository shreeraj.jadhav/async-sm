#ifndef sm_server_session_h
#define sm_server_session_h

#include <comm/service.h>
#include <memory>

namespace sm
{
namespace server
{
class ProxyHandler;
class Session
{
public:
  Session();
  ~Session();

  bool Initialize(const std::string& brokerURL, const std::string& name);
  const comm::Service& GetService() const;

  const ProxyHandler& GetProxyHandler() const;

private:
  Session(const Session&) = delete;
  void operator=(const Session&) = delete;

  class SInternals;
  std::unique_ptr<SInternals> Internals;
  std::shared_ptr<comm::Service> Service;
};
}
}

#endif
