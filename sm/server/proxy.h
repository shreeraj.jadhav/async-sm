#ifndef sm_server_proxy_h
#define sm_server_proxy_h

#include <memory>
#include <sm/common/proxy.pb.h>

class vtkObject;
namespace sm
{
namespace server
{

class Session;

/**
 * @class Proxy
 * @brief mockup for SIProxy
 *
 * Note, Proxy and all its subclasses are just here in place of their ParaView
 * counterparts for the demo. Ultimately, these should be replaced by the
 * ParaView classes and hence the API on these classes has no bearing.
 */

class Proxy
{
public:
  Proxy(vtkObject* obj, const Session& session);
  virtual ~Proxy();

  virtual void SetGlobalID(uint64_t id) { this->GlobalID = id; }
  uint64_t GetGlobalID() const { return this->GlobalID; }

  static std::shared_ptr<Proxy> Create(
    const std::string& xmlgroup, const std::string& xmlname, const Session& session);

  vtkObject* GetVTKObject() const;
  virtual void UpdateState(const sm::common::ProxyState& pstate);
  virtual sm::common::ProxyGenericResponse HandleRequest(
    const sm::common::ProxyGenericRequest& req);

  const Session& GetSession() const;

protected:
  std::shared_ptr<Proxy> LocateProxy(uint64_t gid);

private:
  Proxy(const Proxy&) = delete;
  void operator=(const Proxy&) = delete;

  class PInternals;
  std::unique_ptr<PInternals> Internals;

  uint64_t GlobalID = 0;
};
}
}

#endif
