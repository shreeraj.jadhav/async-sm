#include "proxy.h"

#include <comm/comm.pb.h>
#include <comm/message.h>
#include <comm/service.h>
#include <comm/zmq.h>
#include <sm/client/session.h>

#include <stlab/concurrency/immediate_executor.hpp>
#include <stlab/concurrency/utility.hpp>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

namespace sm
{
namespace client
{

namespace detail
{
void initSphere(std::shared_ptr<Proxy> proxy)
{
  proxy->SetLocations({ "ds" });
  proxy->SetIcon(":/paraview/icons/baseline_adjust_black_24dp.png");
  auto& st = proxy->GetState();
  st.set_xml_group("sources");
  st.set_xml_name("SphereSource");
  auto& propmap = *st.mutable_properties();
  propmap["Center"].add_values()->set_float64(0.0);
  propmap["Center"].add_values()->set_float64(0.0);
  propmap["Center"].add_values()->set_float64(0.0);
  propmap["Radius"].add_values()->set_float64(0.5);
  propmap["ThetaResolution"].add_values()->set_integer(1024);
  propmap["StartTheta"].add_values()->set_float64(0.0);
  propmap["EndTheta"].add_values()->set_float64(360.0);
  propmap["PhiResolution"].add_values()->set_integer(1024);
  propmap["StartPhi"].add_values()->set_float64(0.0);
  propmap["EndPhi"].add_values()->set_float64(180.0);
}

void initWavelet(std::shared_ptr<Proxy> proxy)
{
  proxy->SetLocations({ "ds" });
  proxy->SetIcon(":/paraview/icons/pqExtractGrid24.png");
  auto& st = proxy->GetState();
  st.set_xml_group("sources");
  st.set_xml_name("Wavelet");
  auto& propmap = *st.mutable_properties();
  propmap["WholeExtent"].add_values()->set_integer(-128);
  propmap["WholeExtent"].add_values()->set_integer(128);
  propmap["WholeExtent"].add_values()->set_integer(-128);
  propmap["WholeExtent"].add_values()->set_integer(128);
  propmap["WholeExtent"].add_values()->set_integer(-128);
  propmap["WholeExtent"].add_values()->set_integer(128);

  propmap["Center"].add_values()->set_float64(0.0);
  propmap["Center"].add_values()->set_float64(0.0);
  propmap["Center"].add_values()->set_float64(0.0);
}

void initContour(std::shared_ptr<Proxy> proxy)
{
  proxy->SetLocations({ "ds" });
  proxy->SetIcon(":/paraview/icons/pqIsosurface24.png");
  auto& st = proxy->GetState();
  st.set_xml_group("filters");
  st.set_xml_name("Contour");
  auto& propmap = *st.mutable_properties();
  propmap["IsoValues"].add_values()->set_float64(0.0);
}

void initSlice(std::shared_ptr<Proxy> proxy)
{
  proxy->SetLocations({ "ds" });
  proxy->SetIcon(":/paraview/icons/pqSlice24.png");
  auto& st = proxy->GetState();
  st.set_xml_group("filters");
  st.set_xml_name("Slice");
  auto& propmap = *st.mutable_properties();
  propmap["Origin"].add_values()->set_float64(0.0);
  propmap["Origin"].add_values()->set_float64(0.0);
  propmap["Origin"].add_values()->set_float64(0.0);
  propmap["Normal"].add_values()->set_float64(0.0);
  propmap["Normal"].add_values()->set_float64(0.0);
  propmap["Normal"].add_values()->set_float64(1.0);
}

void initLive(std::shared_ptr<Proxy> proxy)
{
  proxy->SetLocations({ "ds" });
  proxy->SetIcon(":/paraview/icons/pqExtractGrid24.png");
  auto& st = proxy->GetState();
  st.set_xml_group("source");
  st.set_xml_name("Live");
  auto& propmap = *st.mutable_properties();
  propmap["Delay"].add_values()->set_float64(5.0);

  propmap["WholeExtent"].add_values()->set_integer(-128);
  propmap["WholeExtent"].add_values()->set_integer(128);
  propmap["WholeExtent"].add_values()->set_integer(-128);
  propmap["WholeExtent"].add_values()->set_integer(128);
  propmap["WholeExtent"].add_values()->set_integer(-128);
  propmap["WholeExtent"].add_values()->set_integer(128);
}

void initSenseiLive(std::shared_ptr<Proxy> proxy)
{
  proxy->SetLocations({ "ds" });
  proxy->SetIcon(":/paraview/icons/sensei.png");
  auto& st = proxy->GetState();
  st.set_xml_group("source");
  st.set_xml_name("SenseiLive");
  auto& propmap = *st.mutable_properties();
  propmap["Filename"].add_values()->set_str("/tmp/clover.live");
}

void initRepresentationGeometry(std::shared_ptr<Proxy> proxy)
{
  proxy->SetLocations({ "ds", "rs" });
  auto& st = proxy->GetState();
  st.set_xml_name("GeometryRepresentation");
  auto& propmap = *st.mutable_properties();
  propmap["ShowTimeAnnotation"].add_values()->set_integer(0);
}
}

//-----------------------------------------------------------------------------
std::shared_ptr<Proxy> Proxy::createProxy(
  KnownProxies type, std::shared_ptr<sm::client::Session> session)
{
  auto proxy = std::make_shared<Proxy>(session);
  switch (type)
  {
    case SPHERE:
      detail::initSphere(proxy);
      break;

    case WAVELET:
      detail::initWavelet(proxy);
      break;

    case CONTOUR:
      detail::initContour(proxy);
      break;

    case LIVE:
      detail::initLive(proxy);
      break;

    case SENSEILIVE:
      detail::initSenseiLive(proxy);
      break;

    case REPRESENTATION_GEOMETRY:
      detail::initRepresentationGeometry(proxy);
      break;

    case SLICE:
      detail::initSlice(proxy);
      break;
  }
  return proxy;
}

//-----------------------------------------------------------------------------
Proxy::Proxy(std::weak_ptr<sm::client::Session> session)
  : SessionObj(session)
  , Locations{ "ds" }
{
  static std::uint64_t global_id = 0;
  this->State.set_global_id(++global_id);
}

//-----------------------------------------------------------------------------
Proxy::~Proxy()
{
}

//-----------------------------------------------------------------------------
std::uint64_t Proxy::GetGlobalID() const
{
  return this->State.global_id();
}

//-----------------------------------------------------------------------------
void Proxy::setInput(std::shared_ptr<Proxy> proxy)
{
  this->Input = proxy;
  auto& propmap = *this->State.mutable_properties();
  propmap["Input"].add_values()->set_proxy(proxy->GetGlobalID());
  this->UpdateVTKObjects();
}

//-----------------------------------------------------------------------------
void Proxy::UpdateVTKObjects()
{
  auto session = this->SessionObj.lock();
  if (session)
  {
    comm::Message msg;
    msg.Append(this->State);

    // FIXME: all locations efficiently.
    for (const auto& loc : this->Locations)
    {
      comm::Message clone;
      clone.AppendClone(msg);
      session->GetService().SendMessage(loc, std::move(clone));
    }
  }
}

//-----------------------------------------------------------------------------
void Proxy::SendGenericRequest(
  const google::protobuf::Message& body, const std::vector<std::string>& locs_)
{
  auto& locs = locs_.size() > 0 ? locs_ : this->Locations;

  auto session = this->SessionObj.lock();
  if (session)
  {
    sm::common::ProxyGenericRequest req;
    req.set_global_id(this->State.global_id());
    req.mutable_body()->PackFrom(body);

    comm::Message msg;
    msg.Append(req);

    // FIXME: all locations efficiently.
    for (const auto& loc : locs)
    {
      comm::Message clone;
      clone.AppendClone(msg);
      session->GetService().SendMessage(loc, std::move(clone));
    }
  }
}

//-----------------------------------------------------------------------------
std::vector<stlab::future<sm::common::ProxyGenericResponse> > Proxy::SendGenericRequestWithResponse(
  const google::protobuf::Message& body, const std::vector<std::string>& locs_)
{
  std::vector<stlab::future<sm::common::ProxyGenericResponse> > futures;

  auto& locs = locs_.size() > 0 ? locs_ : this->Locations;
  auto session = this->SessionObj.lock();
  if (session)
  {
    sm::common::ProxyGenericRequest req;
    req.set_global_id(this->State.global_id());
    req.mutable_body()->PackFrom(body);

    comm::Message msg;
    msg.Append(req);

    // FIXME: all locations efficiently.
    for (const auto& loc : locs)
    {
      comm::Message clone;
      clone.AppendClone(msg);
      futures.push_back(
        session->GetService().SendRequest(loc, std::move(clone)).then([](const comm::Message& msg) {
          sm::common::ProxyGenericResponse response;
          comm::z2p(*msg.front(), &response);
          return response;
        }));
    }
  }
  return futures;
}

//-----------------------------------------------------------------------------
void Proxy::UpdatePipeline(double time)
{
  auto session = this->SessionObj.lock();
  if (session)
  {
    sm::common::UpdatePipeline up;
    up.set_time(time);
    this->SendGenericRequest(up);
  }
}

//-----------------------------------------------------------------------------
stlab::future<std::string> Proxy::GetDataInformation()
{
  auto session = this->SessionObj.lock();
  if (session)
  {
    sm::common::GatherInformation gatherInfo;
    gatherInfo.set_type("DataInformation");
    auto fvec = this->SendGenericRequestWithResponse(gatherInfo, { "ds" });
    if (fvec.size() == 1)
    {
      auto fmsg = std::move(fvec.front());
      return fmsg.then([](const sm::common::ProxyGenericResponse& response) {
        if (response.body().Is<sm::common::DataInformation>())
        {
          sm::common::DataInformation dinfo;
          response.body().UnpackTo(&dinfo);
          return dinfo.text();
        }
        return std::string();
      });
    }
  }
  return stlab::future<std::string>();
}
}
}
