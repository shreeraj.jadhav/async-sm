TODOs
=====

1. `Service::Subscribe` should reuse subscription for same channel. Thus avoid
   creating multiple streams for same channel.

  - How can we know when all receiver channels are gone, that way we can
    unsubcribed. That should avoid receiving notifcations we no longer care
    about.

2. Since tracking "blockages" on client are going be critical, can we log the
   time it takes for any code on client? That'll help iron out the throttling
   points.

3. We need to add first class support to move "heavy data". A message can
   comprise of VTK data objects. In that case, we don't want to serialize the
   data unless it's being shipped to another process. How can we support it
   gracefully?

4. The rendering happening in "main-loop" is causing a lockup for the "first"
   render when new data shows up since the first render is not as fast as
   subsequent renders.



Serialization
==============

Serialization of data to be "shipped" is an important aspect that warrants a
separate discussion.

`comm::Message` represents the collection of message frames that can be
communicated. When the frame has `light-data`, it can be copied, transmitted
without regards to what type of endpoint the socket is connected to i.e. inproc
or tcp. For `heavy-data` such as vtkDataObject's, however, we want to send
pointers for inproc and serialize using readers/writers for tcp. Our API needs
to support that gracefully.
